close all
%%% Faudrait faire un contrôleur avec des oscillations dedans
for ii=1:2
d=rand;
%mc=d*320+(1-d)*384;
mc=d*200+(1-d)*400;
d=rand;
%mw=d*38+(1-d)*42;
mw=d*30+(1-d)*60;
d=rand;
%kc=d*171+(1-d)*189;
kc=d*100+(1-d)*200;
d=rand;
%kw=d*180+(1-d)*220;
kw=d*150+(1-d)*300;
d=rand;
%c=d*950+(1-d)*1050;
c=d*700+(1-d)*1400;
E=[mc 0 0 0 -1;0 1 0 0 0;0 0 mw 0 1;0 0 0 1 0;0 0 0 0 1];
Ad=[0 0 0 0;1 0 0 0;0 0 0 -kw;0 0 1 0;-c -kc c kc];
Bwd=[0;0;kw;0;0];
Bud=[0;0;0;0;1];
A=E\Ad;A=A(1:4,:); 
Bw=E\Bwd;Bw=Bw(1:4,:); 
Bu=E\Bud;Bu=Bu(1:4,:); 
Cz=[0 1 0 0];
Cy=[0 1 0 -1];

ko=[-1;0;0;0;0];
kc=[ -120.9266
  -19.2958
    1.6825
    1.7899
   -0.4078];
r=[199.8604
  104.0714
   22.1513
   20.5541
    8.1307];
G=[898.9587  998.0250  898.9587  998.0250  142.5942
  998.8442  987.8811  998.8442  987.8811  144.6701
   22.6508   -9.9935   22.6508   -9.9935  -16.0085
    6.4849    6.1390    6.4849    6.1390  -16.0690
    6.1149  -10.2503    6.1149  -10.2503  -30.2833];
g=1*[1e1;1e1;1e1;1e1;1];
sigma=0.01*[1;1;1;1;1];
sim('ijc18sim3')
figure(1),hold on
plot(z,'r')
for jj=1:5
    figure(jj+1),hold on
    plot(k.Time,k.Data(:,jj),'r')
end
g=0*g;
sim('ijc18sim3')
figure(1),hold on
plot(z,'b')
for jj=1:5
    figure(jj+1),hold on
    plot(k.Time,k.Data(:,jj),'b')
end
end