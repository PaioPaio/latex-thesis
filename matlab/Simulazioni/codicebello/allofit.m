clc; close all; clear;

%% what do we want to do ?

%type of system
%1->car suspension, 2->inverted pendulum
wantsys=2;  

%type of controller
wantcontrol=1;

%do we want to do the calculations for G?
wantopt=1;

%which lmi do we want to consider in the optimization ?
% 1->shitty LMI but less demanding, 2->good LMI but waaay more demanding (just use maxder=0 if you want static xi)
wantlmi=2;      

%do we want to simulate ?
wantsim=1;

%do we want to plot ?
wantplot=0;

%do we also want epsilon ?
wanteps=1;

%do we already have a functioning control in nominal case ?
computecontrol=1;

%% polytope and derivatives creation
nu=[4 5];                     %uncertanties we dont want to consider

parameters={};

switch wantsys
    case 1
        
        nxsim=4;   %number of states
        np=1;    %number of auxilliary states
        
        %quarter model car uncertanties
        
        parameters{1}=1.*[320,384];    %mass of the chassis
        % parameters{1}=[200,400];
        
        parameters{2}=1.*[38,42];      %mass of the wheel
        % parameters{2}=[30,53];
        
        parameters{3}=[171,189];    %wheel's stiffness
        % parameters{3}=[140,230];
        
        parameters{4}=[180,220];    %suspension's stiffness
        % parameters{4}=[120,300];
        
        parameters{5}=[950,1050];   %suspension's dampening
        % parameters{5}=[920,1200];
   
     case 2
        
        nxsim=4;   %number of states
        np=5;    %number of auxilliary states
        
        grav=9.81;
        l=0.2;              %pendulum length
        
        %inverted pendulum uncertanties
        parameters{1}=1.*[0.505,0.51];          %mass of the chassis
        
        parameters{2}=1.*[6.55e-2,6.6e-2];   %mass of the pendulum
        
        parameters{3}=[8.7e-4,9e-4];           %pendulum inertia in center of mass
        
        parameters{4}=[1e-3];              %cp viscous coefficient
        
        parameters{5}=[3.6];              %ccx viscous coefficient
        
end

%just take the mean of the uncertanties we do not want
for i=1:length(nu)
    
    parameters{nu(i)}=mean(parameters{nu(i)});
end

paramarray=zeros(5,2);

for i=1:5
    
    if ismember(i,nu)
        
        paramarray(i,1)=parameters{i}(1);
        paramarray(i,2)=parameters{i}(1);
    else
        
        paramarray(i,1)=parameters{i}(1);
        paramarray(i,2)=parameters{i}(2);
    end
end

%index of uncertain parameters
unc=setxor(1:5,nu);
%number of uncertanties
ni=length(unc);   

%vertexes of the polytope of the uncertanties
vertexes=allcomb(parameters{:});
%number of these
nv=size(vertexes,1);        

switch wantlmi
    
    case 1
        %AFFINE xi0 so the total sum will be 1
        
        xi0=randFixedLinearCombination(1,1,ones(1,nv),zeros(1,nv),ones(1,nv));  %starting xi vector

        %matrix of bounds of simplex parameters derivative for affine case
        boundder={};
        maxrand=0.05;
        
        for i=1:nv
            
            der=maxrand*rand(1);
            boundder{i}=[-der,der]';
        end
        
        %matrix with all possible combinations of bounds of \dot(xi)
        %e.g. nv=4 -> [dermax1 dermin2 dermax3 dermax4]
        
        combder=allcomb(boundder{:});           %all the combinations (cell)
        boundder=cell2mat(boundder);            %all the combinations (matrix)
        nd=length(combder);                     %how many combinations
        
    case 2
        
        %MULTIAFFINE xi0 so the sum of each column will be 1 and there will
        %be ni number of columns
        
        xi0=randFixedLinearCombination(ni,1,[1 1],[0 0],[1 1])';
        
        %matrix of bounds of all the ni simplexes parameters derivative for
        %multiaffine case
        boundder={};
        maxrand=0.00;
        bounds=maxrand.*rand(1,ni);
        
        for i=1:ni
            boundder{i}=[-bounds(i),bounds(i)]';
        end
        
        %matrix with all possible combinations of bounds of all the
        %\dot(xi) e.g. ni=2 -> [dermax1 dermin2]
        
        combder=allcomb(boundder{:});
        boundder=cell2mat(boundder);
        nd=length(combder);
end

%% Nominal and Invariant System Matrices

if wantsys==2
    
    Mc=mean(parameters{1});
    mp=mean(parameters{2});
    Jp=mean(parameters{3});
    cp=mean(parameters{4});
    ccx=mean(parameters{5});
    Mtot=mp+Mc;
    W=Jp+mp^2*l^2/Mtot;
    
    Anom=[0 1 0 0;0 -ccx*Jp/(Mtot*W) -mp^2*l^2/(Mtot*W)*grav mp*l*cp/(Mtot*W);...
        0 0 0 1; 0 -mp*l*ccx/(Mtot*W) mp*l*grav/W -cp/W];
    
    Bunom=[0 Jp/(Mtot*W) 0 mp*l/(W*Mtot)]';
    
    Bwnom=[0 (2*mp*l^2-Jp)/(Mtot*W) 0 -(3*mp+2*Mc)*l/(Mtot*W)]';
    
    alpha=2;
    beta=0.5;
    
    tCz=[0 0 alpha beta];
    tCy=[1 0 0 0];
    
    paramean=[Mc mp Jp cp ccx];
else
    
    tCy=[0 1 0 -1];
    tCz=[0 1 0 0];
    
end

%% Linear Controller Matrices

switch wantsys
    
    case 1
    
        switch wantcontrol
        
            case 1
            
                %case with nc=2 and length(diag(k))=5
                
                %eta
                Ak=[0 1; -1 -2];
                B1=[0 0 0 0 0; 1 1 0 0 0];
                B2=[0 1]';
                %y
                C1=[1 0 1 0 0;0 1 0 1 0]';
                D12=[0 0 0 0 1]';
                %\tilde(u)
                C2=zeros(1,2);
                D21=[0 0 1 1 1];
                D22=0;
                
                ko=[-10;0;0;0;0];
                w=[1;1;0.1;0.1;0.1];
            
            case 2
            
                %case with nc=1 and length(diag(k))=3
                
                %eta
                Ak=-1;
                B1=[1 0 0];
                B2=1;
                %y
                C1=[1 1 0]';
                D12=[0 0 1]';
                %\tilde(u)
                C2=0;
                D21=[0 1 1];
                D22=0;
        end
        
    case 2
    
        if computecontrol==1
            
            R2=0.001;
            Q2=diag([0.1 0.1 10 10]);
            R1=0.0001; Q1=1;
            
            sysl=ss(Anom,[Bunom],tCy,[0]);
            sysk=ss(Anom,Bunom,tCy,0);
            ko=lqr(sysk,Q2,R2);
            [not,Lmee,useful]=kalman(sysl,Q1,R1);
            
            %eta
            Ak=(Anom-Lmee*tCy);
            B1=[0 0 0 0;
                Bunom(2) Bunom(2) Bunom(2) Bunom(2);
                0 0 0 0;
                Bunom(4) Bunom(4) Bunom(4) Bunom(4)];
            B2=Lmee;
            %y
            C1=-eye(4);
            D12=zeros(4,1);
            %\tilde(u)
            C2=-ko;
            D21=zeros(1,4);
            D22=0;
            
            hatA=[Anom-Bunom*ko, -Bunom*ko; zeros(4), Anom-Lmee*tCy];
            save('LQG_mats','Ak','B1','B2','C1','D12','C2','D21','D22');
            
        else
            load('LQG_mats');
        end
        
end


nc=size(Ak,2);                       %order of controller


%% System Matrices
switch wantsys
    case 1
        
        
        E2x=full(oio(0,nxsim+nc,np)');
        E2p=full(oio(nxsim+nc,np,0)');
        E2=null(E2p')'*E2x;
        E2E2o=E2*orth(E2');
        
        %invariant matrices
        
        Cy=[D12*tCy C1];
        Cz=[tCz zeros(1,nc)];
        tBu=[0 0 0 0 1000]';
        Dzw=0;
        
        E1xi=cell(1,nv);
        Axi=cell(1,nv);
        Bwxi=cell(1,nv);
        Epixi=[-1 0 1 0 1]';
        ximatrix=cell(nv,5);
        
        
    case 2
        
        
        E2x=full(oio(0,nxsim+nc,np)');
        E2p=full(oio(nxsim+nc,np,0)');
        E2=null(E2p')'*E2x;
        E2E2o=E2*orth(E2');
        
        %invariant matrices
        
        Cy=[D12*tCy C1];
        
        Cz=[tCz zeros(1,nc)];
        Dzw=0;
        
        E1xi=cell(1,nv);
        Axi=cell(1,nv);
        Bwxi=cell(1,nv);
        Buxi=cell(1,nv);
        
        ximatrix=cell(nv,5);    %6->number of matrices to remember
        
end



%build matrices for all vertexes
switch wantsys
    case 1
        for i=1:nv
            
            Exi=[vertexes(i,1) 0 0 0; 0 1 0 0; 0 0 vertexes(i,2) 0; 0 0 0 1; 0 0 0 0];
            
            Axi{i}=[0 0 0 0; 1 0 0 0; 0 0 0 -vertexes(i,4); 0 0 1 0; -vertexes(i,5) -vertexes(i,3) vertexes(i,5) vertexes(i,3)];
            Bwxi{i}=[0 0 vertexes(i,4) 0 0]';
            
            %E1
            E1xi{i}=[Exi,Epixi];
            %Ex
            ximatrix{i,1}=[Exi zeros(nxsim+np,nc); zeros(nc,nxsim) eye(nc)];
            %Epi
            ximatrix{i,2}=[Epixi;zeros(nc,np)];
            %A
            ximatrix{i,3}=[Axi{i}+tBu*D22*tCy tBu*C2; B2*tCy Ak];
            %Bw
            ximatrix{i,4}=[Bwxi{i}; zeros(nc,1)];
            %Bu
            ximatrix{i,5}=[tBu*D21; B1];
        end
    case 2
        for i=1:nv
            %
            %Mc->vertex(1); mp->vertex(2); Jp->vertex(3)
            %cp->vertex(4); ccx->vertex(5)
            %
            Exi=[1 0 0 0; 0 0 0 0; 0 0 1 0; 0 0 0 0; 0 vertexes(i,3) 0 0; 0 vertexes(i,2) 0 0; 0 0 0 vertexes(i,3);...
                0 0 0 vertexes(i,2); 0 0 0 0];
            
            Epixi=[0 0 0 0 0; vertexes(i,1)+vertexes(i,2) vertexes(i,2)*l^2 0 0 grav*l*vertexes(i,2);...
                0 0 0 0 0; 0 0 vertexes(i,1)+vertexes(i,2) l^2*vertexes(i,2) -(vertexes(i,1)+vertexes(i,2))*l*grav;...
                -1 0 0 0 0; 0 -1 0 0 0; 0 0 -1 0 0; 0 0 0 -1 0; 0 0 0 0 1];
            
            Axi{i}=[0 1 0 0; 0 -vertexes(i,3)*vertexes(i,5) 0 vertexes(i,2)*l*vertexes(i,4);...
                0 0 0 1; 0 -vertexes(i,2)*l*vertexes(i,5) 0 -(vertexes(i,2)+vertexes(i,1))*vertexes(i,4);
                0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 0 0; 0 0 vertexes(i,2) 0];
            
            Buxi{i}=[0 vertexes(i,3) 0 vertexes(i,2)*l 0 0 0 0 0]';
            
            Bwxi{i}=[0 -(vertexes(i,3)-2*vertexes(i,2)*l^2) 0 -(3*vertexes(i,2)+2*vertexes(i,1))*l 0 0 0 0 0]';
            
            %E1
            E1xi{i}=[Exi,Epixi];
            %Ex
            ximatrix{i,1}=[Exi zeros(nxsim+np,nc); zeros(nc,nxsim) eye(nc)];
            %Epi
            ximatrix{i,2}=[Epixi;zeros(nc,np)];
            %A
            ximatrix{i,3}=[Axi{i}+Buxi{i}*D22*tCy Buxi{i}*C2; B2*tCy Ak];
            %Bw
            ximatrix{i,4}=[Bwxi{i}; zeros(nc,1)];
            %Bu
            ximatrix{i,5}=[Buxi{i}*D21; B1];
        end
end
%% Sizes
n=size(ximatrix{1,1},1);                            %number of equations
q=size([ximatrix{1,1},ximatrix{1,2}],2);            %number of colums of the decomposition of E
rp=rank(E2p);                                       %rank of E2p
rx=rank(E2);                                        %rank of E2 (nullspace of this will be region of attraction)
mw=size(ximatrix{1,4},2);                           %size of noise's vector
pz=size(Cz,1);                                      %size of external output
p=size(Cy,1);                                       %size of output
nx=size(ximatrix{1},2);
%% Baseline

% weigths on the size of adaptive parameters

w=[1;1;0.1;0.1;0.1];


%% initial conditions
x0=0.01.*[0.1 0.1 0.1 0.1];

%% LMIs

if wantopt==1
    
    sdpopts=sdpsettings('verbose',0,'solver','sdpt3');
    del=1.05;% relaxation of the performance after initialization
    tolg=1e-3;% threshold on gamma
    
    switch wantlmi
        
        case 1 %-> narrow LMI but easy to compute
            
            if maxrand>0
                maxder=max(max(boundder));
            else
                maxder=0;
            end
            
            go2=sdpvar;
            S=sdpvar(q+nx+mw,n);
            quiz=[];
            N1x=oio(0,q,nx+mw);
            N2x=oio(q,nx,mw);
            Nz=[zeros(pz,q),Cz,Dzw];
            Nw=oio(q+nx,mw,0);
            P=sdpvar(q-rp,q-rp,nv);
            
            %initialization LMI ->if this doesnt hold, no candle
            for i=1:nv
                Ma{i}=[ximatrix{i,1}, ximatrix{i,2}, -ximatrix{i,3}-ximatrix{i,5}*diag(ko)*Cy,-ximatrix{i,4}];
                Y=sdpvar(nx,nx-rx,'full');
                Pe=(E2'*P(:,:,i)+Y*null(E2')')*null(E2p')';
                quiz=quiz...
                    +[E2E2o'*P(:,:,i)*E2E2o>=0]...
                    +[maxder*(N2x'*E2'*sum(P,3)*E2*N2x)+(Nz'*Nz)-go2*(Nw'*Nw)+(N2x'*Pe*N1x+S*Ma{i})+(N2x'*Pe*N1x+S*Ma{i})'<=0];
            end
            
            %%% Solve and conclude initialization step
            optimize(quiz,go2,sdpopts);
            pres=checkset(quiz);
            if all(pres>0)
                fprintf('Initialization succeeded with LMI1. go=%g\n',sqrt(double(go2)));
            else
                return
            end
            
            %debug info 
            nbvars(1)=length(getvariables(quiz));
            info=lmiinfo(quiz);nbrows(1)=sum(info.sdp(:,1));
            
            g(1)=sqrt(del*double(go2));
            fi=repmat(ko,1,nv);
            kc=ko;
            r=zeros(p,1);
            %%% BMI problem to be solved
            R2=sdpvar(p,p,'diag');
            Kc=sdpvar(p,p,'diag');
            DKc=sdpvar(p,p,'diag');
            S=sdpvar(q+nx+mw+p,n);
            f=sdpvar(p,nv,'full');
            %eps=sdpvar;
            alf=sdpvar;
            g2=sdpvar;
            G=sdpvar(p,p,'full');
            quiz=[[R2,Kc+DKc-diag(ko);Kc+DKc-diag(ko),eye(p)]>=0]...
                +[-1000*ones(p,p)<=G<=1000*ones(p,p)];
            N1x=oio(0,q,nx+mw+p);
            N2x=oio(q,nx,mw+p);
            Nz=[zeros(pz,q),Cz,Dzw,zeros(pz,p)];
            Nw=oio(q+nx,mw,p);
            Ny=[zeros(p,q), Cy, zeros(p,mw+p)];
            Nyu=[Ny;-oio(q+nx+mw,p,0)];
            P=sdpvar(q-rp,q-rp,nv);
            for i=1:nv
                Ma{i}=[ximatrix{i,1}, ximatrix{i,2}, -ximatrix{i,3}-ximatrix{i,5}*diag(ko)*Cy,-ximatrix{i,4}, -ximatrix{i,5}];
            end
            for v=1:nv
                Y=sdpvar(nx,nx-rx,'full');
                Pe=(E2'*P(:,:,v)+Y*null(E2')')*null(E2p')';
                S1=alf*((Nz'*Nz)-g2*(Nw'*Nw))+2*Ny'*R2*Ny;
                NS1=N2x'*Pe*N1x+S*Ma{i}-Nyu'*[Kc+2*DKc-diag(ko);eye(p)]*[Kc,eye(p)]*Nyu+Ny'*G'*[diag(f(:,v))-diag(ko),eye(p)]*Nyu;
                quiz=quiz...
                    +[E2E2o'*P(:,:,v)*E2E2o>=0]...
                    +[[R2,Kc+DKc-diag(f(:,v));Kc+DKc-diag(f(:,v)),eye(p)]>=0]...
                    +[maxder*(N2x'*E2'*sum(P,3)*E2*N2x)+S1+NS1+NS1'<=0];
                % same as before for esp "eps*(N2x'*E2'*E2*N2x)+"
            end
            nbvars(2)=length(getvariables(quiz));
            info=lmiinfo(quiz);nbrows(2)=sum(info.sdp(:,1));
            %%% Main loop of the heuristic
            cont=1;it=0;beta_0=[0 1 1];
            while cont
                it=it+1;
                % step it,1 - looking for DKc
                quiz_1=replace(quiz,f,fi);
                quiz_1=replace(quiz_1,g2,g(it)^2);
                quiz_2=replace(quiz_1,diag(Kc),kc(:,it));
                %optimize(quiz_2,[],sdpopts);
                optimize(quiz_2,-w'*R2*w,sdpopts);
                pres=checkset(quiz_2);
                fprintf('Residuals for step 1 iteration %f\n',it)
                pres
                fprintf('Radius for step 1 iteration %f\n',it')
                double(R2)
                fprintf('Delta k for step 1 iteration %f\n',it')
                double(DKc)
                fprintf('gamma for step 1 iteration %f\n',it')
                double(g2)
                fprintf('G matrix for step 1 iteration %f\n',it')
                double(G)
                fprintf(' \n')
                if any(pres<-1e-4)
                    fprintf('LMI not feasible while searching for DKc\n');
                    DKci=zeros(p,p);
                    %cont=0;
                else
                    DKci=double(DKc);
                end
                % step it,2 - looking for Kc:=Kc+beta*DKc
                if norm(DKci)<1e-2*norm(diag(kc(:,it)))
                    cont_beta=0;
                    beta=[0 0 0];
                    kc(:,it+1)=kc(:,it);
                    r(:,it+1)=r(:,it);
                else
                    cont_beta=1;
                    beta=beta_0;
                end
                it_beta=0;
                Gi=double(G);
                R2i=double(R2);
                alfi=double(alf);
                while cont_beta
                    it_beta=it_beta+1;
                    quiz_3=replace(quiz_1,diag(Kc),kc(:,it)+beta(2)*diag(DKci));
                    quiz_3=replace(quiz_3,diag(DKc),zeros(p,1));
                    optimize(quiz_3,-w'*R2*w+0.01*alf,sdpopts);
                    pres=checkset(quiz_3);
                    if all(pres>-1e-5)
                        beta(1)=beta(2);
                        kc(:,it+1)=kc(:,it)+beta(1)*diag(DKci);
                        Gi=double(G);
                        R2i=double(R2);
                        r(:,it+1)=sqrt(diag(R2i));
                        alfi=double(alf);
                    else
                        beta(3)=beta(2);
                    end
                    beta(2)=0.5*(beta(3)+beta(1));
                    fprintf('%2.2g ',beta(2));
                    if (beta(2)-beta(1))<0.5 && beta(1)
                        cont_beta=0;
                    elseif it_beta>10
                        disp('bisection gives nothing...')
                        cont_beta=0;
                        kc(:,it+1)=kc(:,it);
                        %             Gi=double(G);
                        %             R2i=double(R2);
                        r(:,it+1)=r(:,it);
                        %             alfi=double(alf);
                    end
                    fprintf('Residuals for step 2 iteration %f\n',it')
                    pres
                    fprintf('Radius for step 2 iteration %f\n',it')
                    double(R2)
                    fprintf('Delta k for step 2 iteration %f\n',it')
                    double(DKc)
                    fprintf('gamma for step 2 iteration %f\n',it')
                    double(g2)
                    fprintf('G matrix for step 2 iteration %f\n',it')
                    double(G)
                    fprintf(' \n')
                end
                if beta(1)
                    beta_0(2:3)=min(2*beta(2:3),[1 1]);
                end
                % step it,3 - looking for Kc:=Kc+beta*DKc
                quiz_4=replace(quiz,diag(DKc),zeros(p,1));
                quiz_4=replace(quiz_4,G,Gi);
                quiz_4=replace(quiz_4,diag(Kc),kc(:,it+1));
                quiz_4=replace(quiz_4,diag(R2),diag(R2i));
                quiz_4=replace(quiz_4,alf,alfi);
                optimize(quiz_4,g2,sdpopts);
                pres=checkset(quiz_4);
                fprintf('Residuals for step 3 iteration %f\n',it')
                pres
                fprintf('Radius for step 3 iteration %f\n',it')
                double(R2)
                fprintf('Delta k for step 3 iteration %f\n',it')
                double(DKc)
                fprintf('gamma for step 3 iteration %f\n',it')
                double(g2)
                fprintf('G matrix for step 3 iteration %f\n',it')
                double(G)
                fprintf(' \n')
                fi=double(f);
                g(it+1)=sqrt(double(g2));
                fprintf('\ng=%g with pres=%g\n',g(it+1),min(pres));
                if g(it)-g(it+1)<tolg
                    cont=0;
                end
            end
            save('LMI1opt','Gi','r','kc');
            
        case 2 %->good LMI but much heavier computation
            
            if maxrand==0
                nd=1;               %consider at least the initial polytope if the derivatives are 0
            end
            
            go2=sdpvar;
            S=sdpvar(q+nx+mw,n);
            N1x=oio(0,q,nx+mw);
            N2x=oio(q,nx,mw);
            Nz=[zeros(pz,q),Cz,Dzw];
            Nw=oio(q+nx,mw,0);
            P=sdpvar(q-rp,q-rp,nv);
            
            if wanteps==1
                eps0=sdpvar;
                quiz=[eps0>=0];
            else
                eps0=0;
                quiz=[];
            end
            
            %initialization LMI -> compute L2 performance with non adaptive
            %control with derivative added
            for i=1:nv
                Ma{i}=[ximatrix{i,1}, ximatrix{i,2}, -ximatrix{i,3}-ximatrix{i,5}*diag(ko)*Cy,-ximatrix{i,4}];
                Y=sdpvar(nx,nx-rx,'full');
                Pe=(E2'*P(:,:,i)+Y*null(E2')')*null(E2p')';
                quiz=quiz+[E2E2o'*P(:,:,i)*E2E2o>=0];
                for j=1:nd
                    quiz=quiz...
                        +[(N2x'*E2'*(Pder(i,P,combder(j,:))+eps0*eye(q-rp))*E2*N2x)+(Nz'*Nz)-go2*(Nw'*Nw)+(N2x'*Pe*N1x+S*Ma{i})+(N2x'*Pe*N1x+S*Ma{i})'<=0];
                end
            end
            
            %%% Solve and conclude initialization step
            optimize(quiz,go2,sdpopts);
            pres=checkset(quiz);
            if all(pres>-1e-6)
                fprintf('Initialization succeeded with LMI2. go=%g\n',sqrt(double(go2)));
            else
                return
            end
            %get number of optimization variables
            nbvars(1)=length(getvariables(quiz));
            info=lmiinfo(quiz);nbrows(1)=sum(info.sdp(:,1));
            %relax performance of the LMI
            g(1)=sqrt(del*double(go2));
            fi=repmat(ko,1,nv);
            kc=ko;
            r=zeros(p,1);
            
            %%% BMI problem to be solved
            aps=double(eps0);
            R2=sdpvar(p,p,'diag');
            Kc=sdpvar(p,p,'diag');
            DKc=sdpvar(p,p,'diag');
            S=sdpvar(q+nx+mw+p,n);
            f=sdpvar(p,nv,'full');
            %eps=sdpvar;
            alf=sdpvar;
            g2=sdpvar;
            G=sdpvar(p,p,'full');
            quiz=[[R2,Kc+DKc-diag(ko);Kc+DKc-diag(ko),eye(p)]>=0]...
                +[-1000*ones(p,p)<=G<=1000*ones(p,p)];
            N1x=oio(0,q,nx+mw+p);
            N2x=oio(q,nx,mw+p);
            Nz=[zeros(pz,q),Cz,Dzw,zeros(pz,p)];
            Nw=oio(q+nx,mw,p);
            Ny=[zeros(p,q), Cy, zeros(p,mw+p)];
            Nyu=[Ny;-oio(q+nx+mw,p,0)];
            P=sdpvar(q-rp,q-rp,nv);
            for i=1:nv
                Ma{i}=[ximatrix{i,1}, ximatrix{i,2}, -ximatrix{i,3}-ximatrix{i,5}*diag(ko)*Cy,-ximatrix{i,4}, -ximatrix{i,5}];
                Y=sdpvar(nx,nx-rx,'full');
                Pe=(E2'*P(:,:,i)+Y*null(E2')')*null(E2p')';
                S1=alf*((Nz'*Nz)-g2*(Nw'*Nw))+2*Ny'*R2*Ny;
                NS1=N2x'*Pe*N1x+S*Ma{i}-Nyu'*[Kc+2*DKc-diag(ko);eye(p)]*[Kc,eye(p)]*Nyu+Ny'*G'*[diag(f(:,i))-diag(ko),eye(p)]*Nyu;
                quiz=quiz...
                    +[[R2,Kc+DKc-diag(f(:,i));Kc+DKc-diag(f(:,i)),eye(p)]>=0]...
                    +[E2E2o'*P(:,:,i)*E2E2o>=0];
                for j=1:nd
                    quiz=quiz...
                        +[(N2x'*E2'*(Pder(i,P,combder(j,:))+eps*eye(q-rp))*E2*N2x)+S1+NS1+NS1'<=0];
                end
                % same as before for esp "eps*(N2x'*E2'*E2*N2x)+"
            end
            nbvars(2)=length(getvariables(quiz));
            info=lmiinfo(quiz);nbrows(2)=sum(info.sdp(:,1));
            %%% Main loop of the heuristic
            cont=1;it=0;beta_0=[0 1 1];
            while cont
                it=it+1;
                % step it,1 - looking for DKc
                quiz_1=replace(quiz,f,fi);
                quiz_1=replace(quiz_1,g2,g(it)^2);
                quiz_2=replace(quiz_1,diag(Kc),kc(:,it));
                %optimize(quiz_2,[],sdpopts);
                optimize(quiz_2,-w'*R2*w,sdpopts);
                pres=checkset(quiz_2);
                %                 fprintf('Residuals for step 1 iteration %f\n',it)
                %                 pres
                %                 fprintf('Radius for step 1 iteration %f\n',it')
                %                 double(R2)
                %                 fprintf('Delta k for step 1 iteration %f\n',it')
                %                 double(DKc)
                %                 fprintf('gamma for step 1 iteration %f\n',it')
                %                 double(g2)
                %                 fprintf('G matrix for step 1 iteration %f\n',it')
                %                 double(G)
                %                 fprintf(' \n')
                if any(pres<-1e-4)
                    fprintf('LMI not feasible while searching for DKc\n');
                    DKci=zeros(p,p);
                    %cont=0;
                else
                    DKci=double(DKc);
                end
                % step it,2 - looking for Kc:=Kc+beta*DKc
                if norm(DKci)<1e-2*norm(diag(kc(:,it)))
                    cont_beta=0;
                    beta=[0 0 0];
                    kc(:,it+1)=kc(:,it);
                    r(:,it+1)=r(:,it);
                else
                    cont_beta=1;
                    beta=beta_0;
                end
                it_beta=0;
                Gi=double(G);
                R2i=double(R2);
                alfi=double(alf);
                while cont_beta
                    it_beta=it_beta+1;
                    quiz_3=replace(quiz_1,diag(Kc),kc(:,it)+beta(2)*diag(DKci));
                    quiz_3=replace(quiz_3,diag(DKc),zeros(p,1));
                    optimize(quiz_3,-w'*R2*w+0.01*alf,sdpopts);
                    %+0.01*alf sulla funzione obbiettivo forse
                    pres=checkset(quiz_3);
                    if all(pres>-1e-5)
                        beta(1)=beta(2);
                        kc(:,it+1)=kc(:,it)+beta(1)*diag(DKci);
                        Gi=double(G);
                        R2i=double(R2);
                        r(:,it+1)=sqrt(diag(R2i));
                        alfi=double(alf);
                    else
                        beta(3)=beta(2);
                    end
                    beta(2)=0.5*(beta(3)+beta(1));
                    fprintf(' %2.2g ',beta(2));
                    if (beta(2)-beta(1))<0.5 && beta(1)
                        cont_beta=0;
                    elseif it_beta>10
                        disp('bisection gives nothing...')
                        cont_beta=0;
                        kc(:,it+1)=kc(:,it);
                        %             Gi=double(G);
                        %             R2i=double(R2);
                        r(:,it+1)=r(:,it);
                        %             alfi=double(alf);
                    end
                    %                     fprintf('Residuals for step 2 iteration %f\n',it')
                    %                     pres
                    %                     fprintf('Radius for step 2 iteration %f\n',it')
                    %                     double(R2)
                    %                     fprintf('Delta k for step 2 iteration %f\n',it')
                    %                     double(DKc)
                    %                     fprintf('gamma for step 2 iteration %f\n',it')
                    %                     double(g2)
                    %                     fprintf('G matrix for step 2 iteration %f\n',it')
                    %                     double(G)
                    %                     fprintf(' \n')
                end
                if beta(1)
                    beta_0(2:3)=min(2*beta(2:3),[1 1]);
                end
                % step it,3 - looking for Kc:=Kc+beta*DKc
                quiz_4=replace(quiz,diag(DKc),zeros(p,1));
                quiz_4=replace(quiz_4,G,Gi);
                quiz_4=replace(quiz_4,diag(Kc),kc(:,it+1));
                quiz_4=replace(quiz_4,diag(R2),diag(R2i));
                quiz_4=replace(quiz_4,alf,alfi);
                optimize(quiz_4,g2,sdpopts);
                pres=checkset(quiz_4);
                if all(pres>-1e-5)
                    fprintf('LMI not feasible while searching for gamma\n');
                end
                %                 fprintf('Residuals for step 3 iteration %f\n',it')
                %                 pres
                %                 fprintf('Radius for step 3 iteration %f\n',it')
                %                 double(R2)
                %                 fprintf('Delta k for step 3 iteration %f\n',it')
                %                 double(DKc)
                %                 fprintf('gamma for step 3 iteration %f\n',it')
                %                 double(g2)
                %                 fprintf('G matrix for step 3 iteration %f\n',it')
                %                 double(G)
                %                 fprintf(' \n')
                fi=double(f);
                g(it+1)=sqrt(double(g2));
                fprintf('\n Step 3 g=%g with pres=%g\n',g(it+1),min(pres));
                if g(it)-g(it+1)<tolg
                    cont=0;
                elseif g(it)-g(it+1)>10
                    fprintf('\n WTF happened \n')
                end
            end
            save('LMI2opt','Gi','r','kc','alfi','g');
    end
    
    
    mini=zeros(1,nv);
    for i=1:nv
        mini(i)=min(eig(double(P(:,:,i))));
    end
    
end
%%
DDelta=sum(max(abs(boundder)));

monten=1;

%% Simulate
if wantsim==1
    switch wantsys
        case 1
            if nc==2
                sigma=0.01*[1;1;1;1;1];
                gsim=1*[1e1;1e1;1e1;1e1;1];
            elseif nc==1
                sigma=0.01*[1;1;1];
                gsim=1*[1e1;1e1;1];
            end
            %get the matrixes for the simulation
            Asim=zeros(nxsim,nxsim,nv);
            Rsim=zeros(nxsim,nxsim,nv);
            Bwsim=zeros(nxsim,nv);
            Busim=zeros(nxsim,nv);
            for i=1:nv
                A1=E1xi{i}\Axi{i};
                Bu1=E1xi{i}\tBu;
                Bw1=E1xi{i}\Bwxi{i};
                Asim(:,:,i)=A1(1:nxsim,:);
                Busim(:,i)=Bu1(1:nxsim,:);
                Bwsim(:,i)=Bw1(1:nxsim,:);
                Rsim(:,:,i)=ctrb(Asim(:,:,i),Busim(:,i));
            end
            
            switch wantlmi
                
                case 1
                    if wantopt~=1
                        load('LMI1opt','Gi','r','kc','alfi','g');
                    end
                    out=sim('simulazioneaffine');
                case 2
                    if wantopt~=1
                        load('LMI2opt','Gi','r','kc','alfi','g');
                    end
                    for i=1:monten
                        x0=10.*rand(nxsim,1);
                        out{i}=sim('simulazionemulti');
                    end
            end
        case 2
    end
end

gamma=g(end);
alfa=double(alfi);
rr=max(r(:,end));
zperf=gamma*out{1}.w2.Data(end)+(8*p*150*(max(sigma)+DDelta)+1)*rr^2/(alfa*min(gsim));

if wantplot==1
    practical=(gamma.*out.w2.Data)+((8*p*(max(sigma)+DDelta)+1)*rr^2/(alfa*min(gsim))).*out.w2.Time;
    figure(1)
    plot(out.z2)
    hold on
    plot(out.z2open)
    plot(out.w2.Time,out.w2.Data.*gamma)
    plot(out.w2.Time,practical)
    legend('||z||_2','||z_open||_2','L_2 norm','Practical L_2 norm')
    
    
    plot()
    title('||z||__2')
    xlabel
end

