function derxi=xider(derbound,xinow,deltaT,caso)


%% function that randomly puts out derivatives for our xi

%cellarrbound-> bounds of said derivatives
%derxi-> vector of xi derivatives
%xinow-> current xi to check against deltaT if the derivative pushes the xi
%out of [0 1]
%caso-> affine or multiaffine case

switch caso
    case 1
        %affine case, the sum of all derivatives must be=0
        n=size(derbound,2);
        %check cellarrbound against xinow and deltaT
        %if the derivative would push xi outsie the polytope obviously we want to
        %avoid that
        for i=1:n
            if (derbound(1,i)*deltaT+xinow(i))<0
                derbound(1,i)=(0-xinow(i))/deltaT;
            end
            if (derbound(2,i)*deltaT+xinow(i))>1
                derbound(2,i)=(1-xinow(i))/deltaT;
            end
        end
        
        %collect upper ad lower bounds
        ubound=zeros(1,n);
        lbound=zeros(1,n);
        for i=1:n
            lbound(i)=derbound(1,i);
            ubound(i)=derbound(2,i);
        end
        
        %the sum of the derivatives should be equal to 0
        derxi=randFixedLinearCombination(1,0,ones(1,n),lbound,ubound);
    case 2
        %multi affine case, super ez, the sum of every pair of derivatives
        %must be =0
        n=size(derbound,2);
        
        derxi=zeros(2,n);
        for i=1:n
            
            if (derbound(1,i)*deltaT+xinow(1,i))<0
                derbound(1,i)=(0-xinow(1,i))/deltaT;
            end
            if (derbound(2,i)*deltaT+xinow(1,i))>1
                derbound(2,i)=(1-xinow(1,i))/deltaT;
            end
            
            derxi(1,i)=derbound(1,i)+(derbound(2,i)-derbound(1,i))*rand();
            derxi(2,i)=-derxi(1,i);
        end
        
end
end
