function A_xi = convexsum(arrayA,xi,caso)
%arrayA-> array of matrices to be summed
%xi-> array of coefficients for the matrix
%caso-> affine or multi-affine case
switch caso
    case 1
        n=length(xi);
        l1=size(arrayA,1);
        l2=size(arrayA,2);
        A_xi=zeros(l1,l2);
        for i=1:n
            A_xi=A_xi+xi(i).*arrayA(:,:,i);
        end
    case 2
        check=length(size(arrayA));
        if check==3
            n=size(arrayA,3);
            l1=size(arrayA,1);
            l2=size(arrayA,2);
            nuncert=size(xi,2);
            A_xi=zeros(l1,l2);
            for i=1:n
                a=arrayA(:,:,i);
                binindex=fliplr([de2bi(i-1),zeros(1,nuncert-length(de2bi(i-1)))])+ones(1,nuncert);
                for j=1:nuncert
                    a=a.*xi(binindex(j),j);
                end
                A_xi=A_xi+a;
            end
        elseif check==2
            l1=size(arrayA,1);
            n=size(arrayA,2);
            nuncert=size(xi,2);
            A_xi=zeros(l1,1);
            for i=1:n
                b=arrayA(:,i);
                binindex=fliplr([de2bi(i-1),zeros(1,nuncert-length(de2bi(i-1)))])+ones(1,nuncert);
                for j=1:nuncert
                    b=b.*xi(binindex(j),j);
                end
                A_xi=A_xi+b;
            end
        end
end
end

