function Pij = Pder(vertexindex,Parray,dercombination)
%this function is only used in multiaffine case


%Pij ->coefficient ij of \dot(P)(xi)
%vertexindex -> index of vertex in affine combination, so a natural number,
%               we want that in multiaffine form so e.g. 2->[1,2]
%Parray->array of all the Ps (with natural index)
%dercombination->row of matrix with all possible combinations of bounds for
%                the derivative

%the flip is used cause allcomb (used to get the dercombination matrix)
%gives , so we have to get them
%matched

%number of uncertanties
nuncert=length(dercombination);

%if we have index=3 and nunc=2, the multiaffine index would be [2,1] so
%[1,0], this is the flipped binary representation of (3-1)=2
binindex=fliplr([d2b(vertexindex-1),zeros(1,nuncert-length(d2b(vertexindex-1)))]);
bintochange=binindex;

bintochange(1)=0;
Pij=dercombination(1).*Parray(:,:,(b2d(fliplr(bintochange))+1));
bintochange(1)=1;
Pij=Pij-dercombination(1).*Parray(:,:,(b2d(fliplr(bintochange))+1));
bintochange=binindex;

%look at paper to see what we do here
for i=2:nuncert
    bintochange(i)=0;
    Pij=Pij+dercombination(i).*Parray(:,:,(b2d(fliplr(bintochange))+1));
    bintochange(i)=1;
    Pij=Pij-dercombination(i).*Parray(:,:,(b2d(fliplr(bintochange))+1));
    bintochange=binindex;
end
end

%functions cause de2bi and bi2de don't work anymore :(
function binary=d2b(decimal)
    %decimal is an integer
    temp=dec2bin(decimal);
    for i=1:length(temp)
        binary(i)=str2num(temp(i));
    end
end

function decimal=b2d(binary)
    %binary is an array
    decimal=0;
    n=length(binary);
    for i=1:n
        decimal=decimal+2^(n-i)*binary(i);
    end
end
