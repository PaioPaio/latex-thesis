%this G is in 5x5
clc; clear; close all;
%function [g,kc,r,Gi,nbvars,nbrows]=ijcALF(ex,tolg)
ex=2;
% Numerical example of the paper
% [g,kc,r,Gi,nbvars,nbrows]=ijcALF(ex,tolg)
%
sdpopts=sdpsettings('verbose',0,'solver','sdpt3');
del=1.05;% relaxation of the performance after initialization
tolg=1e-3;% threshold on gamma
nbvars=0;
nbrows=0;
maxder=0; %bound for dot(xi(t))
rng('shuffle');
%%% Define the system
switch ex
    case 1
        E2x=1;
        E2p=zeros(1,0);
        E2=null(E2p')'*E2x;
        E2E2o=E2*orth(E2');
        E1(:,:,1)=1;
        A(:,:,1)=-1;
        Bw(:,:,1)=1;
        Bu(:,:,1)=1;
        Cz=1;
        Dzw=0;
        Cy=1;
        % Set the baseline SOF control
        ko=0; % column vector
        w=ko; % weigths on the size of adaptive paramters
    case 2
        %% controller
        Ak=[0 1;-1 -2];
        B1=[0 0 0 0 0;1 1 0 0 0];
        B2=[0;1];
        C1=[1 0 1 0 0;0 1 0 1 0]';
        D12=[0 0 0 0 1]';
        C2=zeros(1,2);
        D21=[0 0 1 1 1];
        D22=0;
        
        %% parameters
        t{1}(:,1)=320;
        t{1}(:,2)=384;
        %commented those to try with only 1 uncertanty
%         t{2}(:,1)=38;
%         t{2}(:,2)=42;
%         t{3}(:,1)=171;
%         t{3}(:,2)=189;
%         t{4}(:,1)=180;
%         t{4}(:,2)=220;
%         t{5}(:,1)=950;
%         t{5}(:,2)=1050;
        % compute vertices
        nbt=length(t);
        bv=zeros(nbt,1);
        ridx=zeros(nbt,2);
        ridxtmp=0;
        for pp=1:nbt
            ridx(pp,:)=[ridxtmp+1,ridxtmp+size(t{pp},1)];
            ridxtmp=ridx(pp,2);
            bv(pp)=size(t{pp},2);
        end
        nbpar=ridx(end,end);
        bbv=prod(bv);
        vtx=zeros(nbpar,1);
        for pp=1:nbt
            vtx=addvtx(vtx,t{pp},ridx(pp,:));
        end
        E2x=full(oio(0,6,1)');
        E2p=full(oio(6,1,0)');
        E2=null(E2p')'*E2x;
        E2E2o=E2*orth(E2');
        for v=1:bbv
            for ip=1:nbt
                tv{ip}=vtx(ridx(ip,1):ridx(ip,2),v);
            end
            E1(:,:,v)=[tv{1} 0 0 0 0 0 -1;0 1 0 0 0 0 0;0 0 40 0 0 0 1;0 0 0 1 0 0 0;0 0 0 0 0 0 1;0 0 0 0 1 0 0;0 0 0 0 0 1 0];
            A(:,:,v)=[0 0 0 0 0 0;1 0 0 0 0 0;0 0 0 -200 0 0;0 0 1 0 0 0;-1000 -180 1000 180 0 0;0 0 0 0 0 1;0 1 0 -1 -1 -2];
            Bw(:,:,v)=[0;0;200;0;0;0;0];
            Bu(:,:,v)=[zeros(4,5);0 0 1 1 1;0 0 0 0 0;1 1 0 0 0];
%             E1(:,:,v)=[tv{1} 0 0 0 0 0 -1;0 1 0 0 0 0 0;0 0 tv{2} 0 0 0 1;0 0 0 1 0 0 0;0 0 0 0 0 0 1;0 0 0 0 1 0 0;0 0 0 0 0 1 0];
%             A(:,:,v)=[0 0 0 0 0 0;1 0 0 0 0 0;0 0 0 -tv{4} 0 0;0 0 1 0 0 0;-tv{5} -tv{3} tv{5} tv{3} 0 0;0 0 0 0 0 1;0 1 0 -1 -1 -2];
%             Bw(:,:,v)=[0;0;tv{4};0;0;0;0];
%             Bu(:,:,v)=[zeros(4,5);0 0 1 1 1;0 0 0 0 0;1 1 0 0 0];
        end
        Cz=[0 1 0 0 0 0];
        Dzw=0;
        Cy=[[0;0;0;0;1]*[0 1 0 -1],[1 0;0 1;1 0;0 1;0 0]];
        % Set the baseline SOF control
        ko=[-10;0;0;0;0];
        %ko=[-0.00565009772821481;-164.475707182615;-0.000880054387541309;0.00521061643135578;-0.0534456802744803];
        %ko=[-1.29433925196849e-05;-21.8469089459633;-2.18637337321059e-07;3.99504875960161e-07;1.19263103595054e-07];
        %ko=[0.111871452125153;-72.3724687772075;0.0337312590778671;-0.0510739110246978;-0.0248469404513417];
        %ko=[-3.2625  ; -5.7895 ; -19.9662]; % column vector
        w=[1;1;0.1;0.1;0.1]; % weigths on the size of adaptive parameters
end
% get the size of the data
n=size(E1,1);
q=size(E1,2);
np=size(E2p,2);
rp=rank(E2p);
nx=size(A,2);
rx=rank(E2);
mw=size(Bw,2);
pz=size(Cz,1);
p=size(Cy,1);
nbv=size(A,3);
%%% Compute the L2 performance of the SOF (initialization)
go2=sdpvar;
%eps0=sdpvar;
S=sdpvar(q+nx+mw,n);
quiz=[];
N1x=oio(0,q,nx+mw);
N2x=oio(q,nx,mw);
Nz=[zeros(pz,q),Cz,Dzw];
Nw=oio(q+nx,mw,0);
little=1e-9;
P=sdpvar(q-rp,q-rp,nbv);
for v=1:nbv
    Mc=[E1(:,:,v),-A(:,:,v)-Bu(:,:,v)*diag(ko)*Cy,-Bw(:,:,v)];
    Y=sdpvar(nx,nx-rx,'full');
    Pe=(E2'*P(:,:,v)+Y*null(E2')')*null(E2p')';
    quiz=quiz...
        +[E2E2o'*P(:,:,v)*E2E2o>=0]...
        +[maxder*(N2x'*E2'*sum(P,3)*E2*N2x)+(Nz'*Nz)-go2*(Nw'*Nw)+(N2x'*Pe*N1x+S*Mc)+(N2x'*Pe*N1x+S*Mc)'<=0];
    % there should also be "eps0*(N2x'*E2'*E2*N2x)+" but eps goes to a very
    % high negative number and fuck that
end
%%% Solve and conclude initialization step
optimize(quiz,go2,sdpopts);
pres=checkset(quiz);
if all(pres>0) 
    fprintf('Initialization succeeded. go=%g\n',sqrt(double(go2)));
else
    return
end
nbvars(1)=length(getvariables(quiz));
info=lmiinfo(quiz);nbrows(1)=sum(info.sdp(:,1));
g(1)=sqrt(del*double(go2));
fi=repmat(ko,1,nbv);
kc=ko;
r=zeros(p,1);
%%% BMI problem to be solved
R2=sdpvar(p,p,'diag');
Kc=sdpvar(p,p,'diag');
DKc=sdpvar(p,p,'diag');
S=sdpvar(q+nx+mw+p,n);
f=sdpvar(p,nbv,'full');
%eps=sdpvar;
alf=sdpvar;
g2=sdpvar;
G=sdpvar(p,p,'full');
quiz=[[R2,Kc+DKc-diag(ko);Kc+DKc-diag(ko),eye(p)]>=0]...
     +[-1000*ones(p,p)<=G<=1000*ones(p,p)];
N1x=oio(0,q,nx+mw+p);
N2x=oio(q,nx,mw+p);
Nz=[zeros(pz,q),Cz,Dzw,zeros(pz,p)];
Nw=oio(q+nx,mw,p);
Ny=[zeros(p,q), Cy, zeros(p,mw+p)];
Nyu=[Ny;-oio(q+nx+mw,p,0)];
P=sdpvar(q-rp,q-rp,nbv);
for v=1:nbv
    Mc=[E1(:,:,v),-A(:,:,v)-Bu(:,:,v)*diag(ko)*Cy,-Bw(:,:,v),-Bu(:,:,v)]
    %P=sdpvar(q-rp);
    Y=sdpvar(nx,nx-rx,'full');
    Pe=(E2'*P(:,:,v)+Y*null(E2')')*null(E2p')';
    S1=alf*((Nz'*Nz)-g2*(Nw'*Nw))+2*Ny'*R2*Ny;
    NS1=N2x'*Pe*N1x+S*Mc-Nyu'*[Kc+2*DKc-diag(ko);eye(p)]*[Kc,eye(p)]*Nyu+Ny'*G'*[diag(f(:,v))-diag(ko),eye(p)]*Nyu;
    quiz=quiz...
        +[E2E2o'*P(:,:,v)*E2E2o>=0]...
        +[[R2,Kc+DKc-diag(f(:,v));Kc+DKc-diag(f(:,v)),eye(p)]>=0]...
        +[maxder*(N2x'*E2'*sum(P,3)*E2*N2x)+S1+NS1+NS1'<=0];
    % same as before for esp "eps*(N2x'*E2'*E2*N2x)+"
end
nbvars(2)=length(getvariables(quiz));
info=lmiinfo(quiz);nbrows(2)=sum(info.sdp(:,1));
%%% Main loop of the heuristic
cont=1;it=0;beta_0=[0 1 1];
while cont
    it=it+1;
    % step it,1 - looking for DKc
    quiz_1=replace(quiz,f,fi);
    quiz_1=replace(quiz_1,g2,g(it)^2);
    quiz_2=replace(quiz_1,diag(Kc),kc(:,it));
    %optimize(quiz_2,[],sdpopts);
    optimize(quiz_2,-w'*R2*w,sdpopts);
    pres=checkset(quiz_2);
    fprintf('Residuals for step 1 iteration %f\n',it)
    pres
    fprintf('Radius for step 1 iteration %f\n',it')
    double(R2)
    fprintf('Delta k for step 1 iteration %f\n',it')
    double(DKc)
    fprintf('gamma for step 1 iteration %f\n',it')
    double(g2)
    fprintf('G matrix for step 1 iteration %f\n',it')
    double(G)
    fprintf(' \n')
    if any(pres<-1e-4) 
        fprintf('LMI not feasible while searching for DKc\n');
        DKci=zeros(p,p);
        %cont=0;
    else
        DKci=double(DKc);
    end
    % step it,2 - looking for Kc:=Kc+beta*DKc
    if norm(DKci)<1e-2*norm(diag(kc(:,it)))
        cont_beta=0;
        beta=[0 0 0];
        kc(:,it+1)=kc(:,it);
        r(:,it+1)=r(:,it);
    else
        cont_beta=1;
        beta=beta_0;
    end
    it_beta=0;
    Gi=double(G);
    R2i=double(R2);
    alfi=double(alf);
    while cont_beta
        it_beta=it_beta+1;
        quiz_3=replace(quiz_1,diag(Kc),kc(:,it)+beta(2)*diag(DKci));
        quiz_3=replace(quiz_3,diag(DKc),zeros(p,1));
        optimize(quiz_3,-w'*R2*w+0.01*alf,sdpopts);
        pres=checkset(quiz_3);
        if all(pres>-1e-5)
            beta(1)=beta(2);
            kc(:,it+1)=kc(:,it)+beta(1)*diag(DKci);
            Gi=double(G);
            R2i=double(R2);
            r(:,it+1)=sqrt(diag(R2i));
            alfi=double(alf);
        else
            beta(3)=beta(2);
        end
        beta(2)=0.5*(beta(3)+beta(1));
        fprintf('%2.2g ',beta(2));
        if (beta(2)-beta(1))<0.5 && beta(1)
            cont_beta=0;
        elseif it_beta>10
            disp('bisection gives nothing...')
            cont_beta=0;
            kc(:,it+1)=kc(:,it);
%             Gi=double(G);
%             R2i=double(R2);
            r(:,it+1)=r(:,it);
%             alfi=double(alf);
        end
    fprintf('Residuals for step 2 iteration %f\n',it')
    pres
    fprintf('Radius for step 2 iteration %f\n',it')
    double(R2)
    fprintf('Delta k for step 2 iteration %f\n',it')
    double(DKc)
    fprintf('gamma for step 2 iteration %f\n',it')
    double(g2)
    fprintf('G matrix for step 2 iteration %f\n',it')
    double(G)
    fprintf(' \n')
    end
    if beta(1)
        beta_0(2:3)=min(2*beta(2:3),[1 1]);
    end
    % step it,3 - looking for Kc:=Kc+beta*DKc
    quiz_4=replace(quiz,diag(DKc),zeros(p,1));
    quiz_4=replace(quiz_4,G,Gi);
    quiz_4=replace(quiz_4,diag(Kc),kc(:,it+1));
    quiz_4=replace(quiz_4,diag(R2),diag(R2i));
    quiz_4=replace(quiz_4,alf,alfi);
    optimize(quiz_4,g2,sdpopts);
    pres=checkset(quiz_4);
    fprintf('Residuals for step 3 iteration %f\n',it')
    pres
    fprintf('Radius for step 3 iteration %f\n',it')
    double(R2)
    fprintf('Delta k for step 3 iteration %f\n',it')
    double(DKc)
    fprintf('gamma for step 3 iteration %f\n',it')
    double(g2)
    fprintf('G matrix for step 3 iteration %f\n',it')
    double(G)
    fprintf(' \n')
    fi=double(f);
    g(it+1)=sqrt(double(g2));
    fprintf('\ng=%g with pres=%g\n',g(it+1),min(pres));
    if g(it)-g(it+1)<tolg
        cont=0;
    end
end

%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function v=addvtx(v,tp,rp)
nbvtx=size(v,2);
v=repmat(v,1,1,size(tp,2));
tp=reshape(tp,size(tp,1),1,size(tp,2));
v(rp(1):rp(2),:,:)=repmat(tp,1,nbvtx,1);
v=reshape(v,size(v,1),[]);
end