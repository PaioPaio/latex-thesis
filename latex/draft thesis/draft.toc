\contentsline {chapter}{\numberline {1}Theory Recall}{9}%
\contentsline {section}{\numberline {1.1}Stability}{9}%
\contentsline {subsection}{\numberline {1.1.1}Lyapunov's Theory}{9}%
\contentsline {subsection}{\numberline {1.1.2}Linear Matrix Inequalities}{12}%
\contentsline {subsection}{\numberline {1.1.3}S-Variable Approach}{13}%
\contentsline {subsection}{\numberline {1.1.4}Performance Evaluation of Systems}{16}%
\contentsline {section}{\numberline {1.2}Descriptor Form}{20}%
\contentsline {subsection}{\numberline {1.2.1}Systems in Descriptor Form}{20}%
\contentsline {subsection}{\numberline {1.2.2}Stability of Descriptor Systems}{23}%
\contentsline {section}{\numberline {1.3}Polytopic Systems and Robust Stability}{26}%
\contentsline {subsection}{\numberline {1.3.1}Polytopes and Polytopic Systems}{26}%
\contentsline {subsection}{\numberline {1.3.2}Robust Stability}{30}%
\contentsline {subsection}{\numberline {1.3.3}Robust Performance Analysis}{31}%
\contentsline {subsection}{\numberline {1.3.4}Robust Stability in Descriptor Form}{32}%
\contentsline {chapter}{\numberline {2}Problem Statement}{35}%
\contentsline {section}{\numberline {2.1}System and Objective}{35}%
\contentsline {section}{\numberline {2.2}The Control Law}{36}%
\contentsline {section}{\numberline {2.3}Rewriting of the System Equations}{37}%
\contentsline {section}{\numberline {2.4}Previous Results}{40}%
\contentsline {subsection}{\numberline {2.4.1}Optimization Algorithm}{42}%
\contentsline {chapter}{\numberline {3}Main Results}{45}%
\contentsline {section}{\numberline {3.1}Assumptions on the Polytope}{45}%
\contentsline {section}{\numberline {3.2}First Set of Conditions}{49}%
\contentsline {section}{\numberline {3.3}Second Set of Conditions}{57}%
\contentsline {section}{\numberline {3.4}Performances and Stability}{60}%
\contentsline {subsection}{\numberline {3.4.1}Practical Stability}{61}%
\contentsline {subsection}{\numberline {3.4.2}Practical $L_{2}$ Performance}{62}%
\contentsline {chapter}{\numberline {4}Example of Procedure}{65}%
\contentsline {section}{\numberline {4.1}Modeling}{65}%
\contentsline {section}{\numberline {4.2}Controller}{69}%
\contentsline {chapter}{\numberline {5}Conclusion}{71}%
