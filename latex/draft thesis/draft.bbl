\begin{thebibliography}{1}

\bibitem{CaratheodoryTheoremConvex2020}
Carath\'eodory's theorem (convex hull).
\newblock {\em Wikipedia}, September 2020.

\bibitem{Polytope2020}
Polytope.
\newblock {\em Wikipedia}, April 2020.

\bibitem{boydLinearMatrixInequalities}
Stephen Boyd, Eric Feron, Laurent El~Ghaoui, and Venkataramanan Balakrishnan.
\newblock {\em Linear Matrix Inequalities in System and Control Theory},
  volume~15 of {\em Studies in {{Applied}} and {{Numerical}}}.
\newblock Siam edition.

\bibitem{ebiharaSVariableApproachLMIBased2015}
Yoshio Ebihara, Dimitri Peaucelle, and Denis Arzelier.
\newblock {\em S-{{Variable Approach}} to {{LMI}}-{{Based Robust Control}}}.
\newblock Communications and {{Control Engineering}}. {Springer London},
  {London}, 2015.

\bibitem{hassank.khalilNonlinearSystems3rd}
Hassan~K. Khalil.
\newblock Nonlinear {{Systems}} (3rd {{Edition}}).

\bibitem{peaucelleAdaptiveControlDesign2018}
Dimitri Peaucelle and Harmony Leduc.
\newblock Adaptive control design with {{S}}-variable {{LMI}} approach for
  robustness and {{L}} {\textsubscript{2}} performance.
\newblock {\em International Journal of Control}, pages 1--10, November 2018.

\bibitem{rantzerKalmanYakubovichPopov1996}
Anders Rantzer.
\newblock On the {{Kalman}}\textemdash{{Yakubovich}}\textemdash{{Popov}} lemma.
\newblock {\em Systems \& Control Letters}, 28(1):7--10, June 1996.

\bibitem{vergheseGeneralizedStatespaceSingular1981}
G.~Verghese, B.~Levy, and T.~Kailath.
\newblock A generalized state-space for singular systems.
\newblock {\em IEEE Transactions on Automatic Control}, 26(4):811--831, August
  1981.

\end{thebibliography}
