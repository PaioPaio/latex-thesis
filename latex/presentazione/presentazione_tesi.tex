\documentclass{beamer}
\usepackage{graphicx,caption}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{microtype}
\usepackage{xcolor}


\graphicspath{ {./immagini/} }

\title{LMI-Based Adaptive Control with Time-Varying Uncertainties}
\author[]{Candidate: Lorenzo Paiola\\[5mm]{\hspace{12mm}Supervisors: Luca Zaccarian (Unitn)\\ \hspace{46mm} Dimitri Peaucelle (LAAS-CNRS)}}
%\author{\texorpdfstring{Lorenzo Paiola\\Relators: Luca Zaccarian (Unitn)\\ Dimitri Peaucelle (LAAS-CNRS)}{Candidato}}
\institute[UniTrento]{Università degli Studi di Trento}
\logo{\includegraphics[width=7mm]{unitn_logo}}
\date{\today}

\usetheme{Berlin}

\begin{document}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}


\begin{frame}
    \maketitle
\end{frame}

\begin{frame}
    \frametitle{Justification}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \includegraphics[width=\columnwidth]{satellite_cnrs.jpg}
            \hspace*{15pt}\hbox{\scriptsize Credit:\thinspace{\small CNES}}
        \end{column}
        \begin{column}{0.5\textwidth}
            Need of a framework to model and control systems with uncertain parameters while mantaining some performance 
            index.
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{Tools Utilized}
    \begin{itemize}
        \item S-Variable Linear Matrix Inequalities
        \item Descriptor Form
        \item Polytopic Uncertain Systems
        \item Adaptive Control
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{S-Variable LMI}
    \framesubtitle{What is a LMI ?}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{equation*}
                F(x)\triangleq F_{0}+\sum_{i=1}^{m}x_{i}F_{i}>0,
            \end{equation*}
            \begin{block}{The Lyapunov's Theorem is an LMI}
                \begin{equation*}
                    P>0,\ A^{T}P+PA<0
                \end{equation*}
            \end{block}
        \end{column}
        \begin{column}{0.4\textwidth}
            Why use LMIs ?
            \begin{itemize}
                \item Many problems in control theory can be re-stated as LMIs
                \item Linear Convex Optimization 
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}
\begin{frame}
    \frametitle{S-Variable LMI}
    \framesubtitle{S-Variable Approach}
    \begin{theorem}[S-Procedure]
        For given $\Theta,\Pi\in\mathbbm{S}^{n}$, the following two conditions are equivalent:
        \begin{enumerate}
            \item $\xi^{T}\Theta\xi<0$ $\forall\xi\not=0$ such that $\xi^{T}\Pi\xi\leq0$
            \item $\exists \tau >0$ such that $\Theta-\tau\Pi<0$.
        \end{enumerate}
    \end{theorem}
\end{frame}
\begin{frame}
    \frametitle{S-Variable LMI}
    \framesubtitle{S-Variable Approach}
    The approach can be used to derive new conditions for stability
    \begin{columns}
        \begin{column}{0.35\textwidth}
            \begin{align*}
                &P>0,\\ &A^{T}P+PA<0
            \end{align*}
        \end{column}
        \begin{column}{0.05\textwidth}
            $\iff$
        \end{column}
        \begin{column}{0.6\textwidth}
              \begin{align*}
                &P>0\\
                  &\begin{bmatrix}
                      0 & P \\
                      P & 0
                  \end{bmatrix}+
                  He\bigg\{\begin{bmatrix}
                      F_{1} \\F_{2}
                  \end{bmatrix}\begin{bmatrix}
                      A & -I
                  \end{bmatrix}\bigg\}<0
              \end{align*}
        \end{column}
    \end{columns}
    \begin{block}{Additional matrix variables}
        $F_{1}$, $F_{2}\in\mathbbm{R}^{n\times n}$ reduce conservativeness.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Descriptor Form}
    \begin{equation*}
        E_{xx}\dot{x}(t)+E_{x\pi}\pi(t)=Ax(t)+Bu(t)   
    \end{equation*}
    \begin{itemize}
        \item Introduces a new auxiliary variable $\pi(t)$
        \item Better suited to describe LTI systems with constraints
        \item Expands the scope of the polytopic modeling
    \end{itemize}
    \begin{block}{Remark}
        If $E_{1}=[E_{xx}\ E_{x\pi}]$ is invertible we can retrieve the canonical state form 
        \begin{equation*}
            \underbrace{E_{1}^{-1}[E_{xx}\dot{x}+E_{x\pi}\pi]}_{=\begin{bmatrix}
                \dot{x} \\ \pi
            \end{bmatrix}}
            =\underbrace{E_{1}^{-1}A}_{=\begin{bmatrix}A \\A_{\pi}
                \end{bmatrix}}x+
            \underbrace{E_{1}^{-1}B}_{=\begin{bmatrix}
                    B \\B_{u}
                \end{bmatrix}}u
        \end{equation*}
    \end{block}
\end{frame}
\begin{frame}
    \frametitle{Descriptor Form}
    \framesubtitle{Example: Quarter Car Model}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{equation*}
                \resizebox{.9\hsize}{!}{$\dot{x}(t)=\begin{bmatrix}
                    0                      & 1            & 0                & 0            \\
                    -\frac{k_{1}+k_{2}}{m} & -\frac{c}{m} & \frac{k_{1}}{m}  & \frac{c}{m}  \\
                    0                      & 0            & 0                & 1            \\
                    \frac{k_{1}}{M}        & \frac{c}{M}  & -\frac{k_{1}}{M} & -\frac{c}{M}
                \end{bmatrix}x+
                \begin{bmatrix}
                    0 \\-\frac{1}{m}\\0\\\frac{1}{M}
                \end{bmatrix}u$}
            \end{equation*}
            Where $x=\begin{bmatrix} z_{w}\\ \dot{z}_{w}\\ z_{c}\\ \dot{z}_{c} \end{bmatrix}$ and $u=f$.
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{quarter.png}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Descriptor Form}
    \framesubtitle{Example: Quarter Car Model}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            In standard Descriptor form
            \begin{equation*}
                \resizebox*{.9\hsize}{!}{$
                \begin{bmatrix}
                    1 & 0 & 0 & 0 \\
                    0 & m & 0 & 0 \\
                    0 & 0 & 1 & 0 \\
                    0 & 0 & 0 & M
                \end{bmatrix}\dot{x}=
                \begin{bmatrix}
                    0              & 1  & 0      & 0  \\
                    -(k_{1}+k_{2}) & -c & k_{1}  & c  \\
                    0              & 0  & 1      & 0  \\
                    k_{1}          & c  & -k_{1} & -c
                \end{bmatrix}x+
                \begin{bmatrix}
                    0 \\-1\\0\\1
                \end{bmatrix}u
                $}
            \end{equation*}
            Introducing the auxiliary variables
            \begin{equation*}
                \underbrace{\begin{bmatrix}
                    \pi_{k_{1}} \\ \pi_{k_{2}}\\ \pi_{c}
                \end{bmatrix}}_{\pi}=
            \begin{bmatrix}
                -k_{1} & 0  & k_{1} & 0 \\
                k_{2}  & 0  & 0     & 0 \\
                0      & -c & 0     & c
            \end{bmatrix}x
            \end{equation*}
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{quarter.png}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Descriptor Form}
    \framesubtitle{Example: Quarter Car Model}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            Leads to
            \begin{equation*}
                \resizebox*{.9\hsize}{!}{$
                \begin{bmatrix}
                    1 & 0 & 0 & 0 \\
                    0 & m & 0 & 0 \\
                    0 & 0 & 1 & 0 \\
                    0 & 0 & 0 & M \\
                    0 & 0 & 0 & 0 \\
                    0 & 0 & 0 & 0 \\
                    0 & 0 & 0 & 0
                \end{bmatrix}\dot{x}+
                \begin{bmatrix}
                    0  & 0 & 0  \\
                    -1 & 1 & -1 \\
                    0  & 0 & 0  \\
                    1  & 0 & 1  \\
                    1  & 0 & 0  \\
                    0  & 1 & 0  \\
                    0  & 0 & 1
                \end{bmatrix}\pi=
                \begin{bmatrix}
                    0      & 1  & 0     & 0 \\
                    0      & 0  & 0     & 0 \\
                    0      & 0  & 1     & 0 \\
                    0      & 0  & 0     & 0 \\
                    -k_{1} & 0  & k_{1} & 0 \\
                    k_{2}  & 0  & 0     & 0 \\
                    0      & -c & 0     & c
                \end{bmatrix}x+
                \begin{bmatrix}
                    0 \\-1\\0\\1\\0\\0\\0
                \end{bmatrix}u
                $}
            \end{equation*}
            \begin{block}{Remark}
                This representation in not unique.
            \end{block}
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{quarter.png}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Descriptor Form}
    \framesubtitle{Stability}
    \begin{theorem}
        Let there exist the following factorization $[E_{xx}\ E_{x\pi}]=E_{1}[E_{2xx}\ E_{2x\pi}]$  where $E_{1}$ is full column rank and let
        $E_{2}=E_{2x\pi}^{\perp}E_{2xx}$. The system in descriptor form is stable if there exist matrices $P\in\mathbbm{S}^{n_{x}}$
        and $Y$ of appropriate dimensions such that the following LMI conditions hold:
        \begin{subequations}
        \begin{align}
            \label{descrstab}
             & (E_{2}E_{2}^{o})^{T}P(E_{2}E_{2}^{o})>0                 \\
             & \begin{bmatrix}
                E_{1} & -A
            \end{bmatrix}^{T\perp}
            \begin{bmatrix}
                0     & P_{e}^{T} \\
                P_{e} & 0
            \end{bmatrix}
            \begin{bmatrix}
                E_{1} & -A
            \end{bmatrix}^{T\perp T}<0                               \\
             & P_{e}=(E_{2}^{\perp}P+Y^{T}E_{2}^{\perp})E_{2x\pi}^{\perp}
        \end{align}
    \end{subequations}
    \end{theorem}
\end{frame}

\begin{frame}
    \frametitle{Polytopic Uncertain Systems}
    \framesubtitle{Affine Polytopic Systems}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            The Standard Simplex is defined as
            \begin{equation*}
                \mathbbm{E}^{v} : = \{\textcolor{black}{\xi} \in \mathbbm{R}^{v}|\textcolor{black}{\xi}\geq0,\mathbbm{1}^T\textcolor{black}{\xi}=1\}
            \end{equation*}
            And can be used to define an affine model set
            \begin{equation*}
                A(\xi)=\sum_{j=1}^{v}\xi_{j}A^{[j]}
            \end{equation*}
            \begin{equation*}
                \dot{x}(t)=A(\xi)x(t),\ \ \xi\in\mathbbm{E}^{v}
            \end{equation*}
            Where $v=2^{n_{i}}$ and $n_{i}$ is the number of uncertain parameters.
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{polysystem.png}
            \hspace*{5pt}\hbox{\scriptsize \thinspace{\small Affine Model Set.}}
        \end{column}
    \end{columns}
\end{frame}
\begin{frame}
    \frametitle{Polytopic Uncertain Systems}
    \framesubtitle{Multiaffine Polytopic Systems}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            A multiaffine model set can be defined as
            \begin{equation*}
                \resizebox*{.9\hsize}{!}{$
                A(\phi_{1},\dots ,\phi_{n_{i}})
                =\sum_{j_{1}=1}^{2}\sum_{j_{2}=1}^{2}\dots\sum_{j_{n_{i}}=1}^{2}\phi_{1j_{1}}\phi_{2j_{2}}\dots\phi_{n_{i}j_{n_{i}}}A^{[j_{1},\dots ,j_{n_{i}}]}
                $}
            \end{equation*}
            Where each $\phi_{i}$ a 2-d vector inside a simplex
            \begin{equation*}
                \phi_{i}\in\mathbb{E}^{2}\ \forall i\in\mathbbm{I}_{n_{i}}
            \end{equation*}
            The number of vertexes remains the same: the number of all possible combinations of $[\phi_{1}\dots \phi_{n_{i}}]$ is 
            $2^{n_{i}}$.
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{multiaffine.png}
            \hspace*{5pt}\hbox{\scriptsize \thinspace{\small Multiaffine Model Set.}}
        \end{column}
    \end{columns}
\end{frame}
%\begin{frame}
%    \frametitle{Polytopic Uncertain Systems}
%    \framesubtitle{Multiaffine vs Affine}
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%            Multiaffine
%            \begin{itemize}
%                \item Directly correlates its parameters to their physical meaning, the uncertain parameter 
%                      $p_{1}\in [\underline{p}_{1}\ \overline{p}_{1}]$ is $p_{1}=\phi_{11}\overline{p_{1}}+\phi_{12}\underline{p_{1}}$
%                \item Unique representation
%                \item Heavier notation
%            \end{itemize}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            Affine
%            \begin{itemize}
%                \item No way of correlating parameters to physical meaning, $\xi$ is only a vector of weighting factors for the vertexes
%                \item Not a unique representation (Caratheodory's Theorem)
%                \item Easier
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
\begin{frame}
    \frametitle{Polytopic Uncertain Systems}
    \framesubtitle{Rationally Dependent Uncertain Systems, from {\tiny Ebihara, Yoshio, Dimitri Peaucelle, e Denis Arzelier. S-Variable Approach to LMI-Based Robust Control. Communications and Control Engineering. London: Springer London, 2015.
    }}
    \begin{theorem}
        Assume a parameter-dependent descriptor model 
        \begin{equation*}
            \tilde{E}_{xx}(\xi)\dot{x}+\tilde{E}_{x\pi}(\xi)\pi=\tilde{A}(\xi)x+\tilde{B}(\xi)u,\ \xi\in\mathbbm{R}^{v}
        \end{equation*} 
        in which the $\xi$-dependent matrices are rational with respect to the components of $\xi$, then, there always 
        exists another parameter-dependent descriptor model 
        \begin{equation*}
            E_{xx}(\xi)\dot{x}+E_{x\pi}(\xi)\pi=A(\xi)x+B(\xi)u,\ \xi\in\mathbbm{R}^{v}
        \end{equation*}
        in which the $\xi$-dependent matrices are affine functions of $\xi$.
    \end{theorem}
\end{frame}
\begin{frame}
    \frametitle{Polytopic Uncertain Systems}
    \framesubtitle{Robust Stability}
    A polytopic system is robustly stable if it's stable for all $\xi\in\mathbbm{E}^{v}$.
    \\
    Checking for $eig(A(\xi))$ over the whole set $\mathbbm{E}^{v}$ is bad even when $\xi$ is constant.
    \begin{theorem}
        The uncertain system $\dot{x}(t)=A(\xi)x(t)$ is stable $\forall\xi\in\mathbbm{E}^{v}$ if there exists a matrix $P\in\mathbbm{S}^{n}$
    such that
        \begin{equation*}
            \begin{array}{cc}
                P>0, & PA^{[j]}+A^{[j]^{T}}P<0\ \forall\ j\in\mathbb{I}_{v}.
            \end{array}
        \end{equation*}
    \end{theorem} 
    The result is still conservative ($\rightarrow\not=\iff$).
\end{frame}
\begin{frame}
    \frametitle{Polytopic Uncertain Systems}
    \framesubtitle{SV-LMI to reduce conservativeness}
    \begin{theorem}
        The uncertain LTI system $\dot{x}(t)=A(\xi)x(t)$ is stable for all $\xi\in\mathbbm{E}^{v}$ if there exist $P^{[j]}\in\mathbbm{S}^{n}$ $(j\in\mathbbm{I}_{v})$, 
        $F_{1}$, $F_{2}\in\mathbbm{R}^{n\times n}$ such that 
        \begin{equation*}
            P^{[j]}>0,\ \ 
            \begin{bmatrix}
                0&P^{[j]}\\
                P^{[j]}&0
            \end{bmatrix}+
            \text{He}\bigg\{
                \begin{bmatrix}
                    F_{1}\\F_{2}
                \end{bmatrix}
                \begin{bmatrix}
                    I&-A^{[j]}
                \end{bmatrix}\bigg\}<0,\ j\in\mathbbm{I}_{v}.
        \end{equation*}
    \end{theorem}
    \begin{block}{Remark}
        The same reasoning can be applied to the results in descriptor form.
    \end{block}
\end{frame}
%\begin{frame}
%    \frametitle{Polytopic Uncertain Systems}
%    \framesubtitle{Robust Stability of Descriptor Form}
%    We analyze now a polytopic descriptor system in the form
%    \begin{equation*}
%        E_{xx}(\xi)\dot{x}+E_{x\pi}\pi=A(\xi)x
%    \end{equation*}
%    \begin{block}{Assumption}
%        Assuming that the matrix $\begin{bmatrix} E_{xx}(\xi) & E_{x\pi}(\xi) \end{bmatrix}$ can be factorized as $E_{1}(\xi)\begin{bmatrix}E_{2xx} & E_{2x\pi}
%        \end{bmatrix}$, we define $E_{1}(\xi)=\sum_{j}^{v}\xi_{i}E_{1}^{[j]}\in \mathbbm{R}^{n\times q}$ a matrix that is full column rank for all
%        $\xi \in \mathbbm{E}^{v}$.
%    \end{block}
%\end{frame}
%\begin{frame}
%    \frametitle{Polytopic Uncertain Systems}
%    \framesubtitle{Robust Stability of Descriptor Form}
%    \begin{theorem}
%        If this Assumption holds, let $E_{2}=E_{2x\pi}^{\perp}E_{2xx}$. The polytopic descriptor system is 
%        robustly stable (stable for all $\xi\in\mathbbm{E}^{v}$) if there exist $P^{[j]}=P^{[j]^{T}}$, $Y^{[j]}$, $F_{1}$ and $F_{2}$ 
%        such that for all $j\in\mathbbm{I}_{v}$:
%        \begin{align*}
%            &(E_{2}E_{2}^{\circ})^{T}P^{[j]}(E_{2}E_{2}^{\circ})>0,\nonumber\\
%            &\begin{bmatrix}
%                0&P_{e}^{[j]^{T}}\\
%                P_{e}^{[j]}&0
%            \end{bmatrix}+
%            He\bigg\{
%                \begin{bmatrix}
%                    F_{1}\\ F_{2}
%                \end{bmatrix}
%                \begin{bmatrix}
%                    E_{1x}^{[j]}&-A^{[j]}
%                \end{bmatrix}
%                \bigg\}<0,\\
%            &P_{e}^{[j]}=(E_{2}^{T}P^{[j]}+Y^{[j]^{T}}E_{2}^{\perp})E_{2x\pi}^{\perp}.\nonumber
%        \end{align*}
%    \end{theorem}
%\end{frame}


\begin{frame}
    \frametitle{Problem Statement}
    The system in analysis will be
        \begin{align}
            \label{sistema}
            & E_{x}(\textcolor{black}{\xi})\dot{x}(t)+E_{\pi}(\textcolor{black}{\xi})\pi(t)=A(\textcolor{black}{\xi})x(t)+B_{w}w(t)+B_{u}(\textcolor{black}{\xi})u(t)   \nonumber\\
            & y(t)=C_{y}x(t),\, \ \ \ \ \ z(t)=C_{z}x(t)+D_{zw}w(t)                                                                                                     \nonumber        \\
            & u(t)=K_{o}y(t)
       \end{align}
    We want to replace the LTI controller with a adaptive dynamic one.
    \begin{block}{Assumption}
        The system is square (p=$m_{u}$, where $y\in\mathbbm{R}^{p}$ and $u\in\mathbbm{R}^{m_{u}}$), and $K_{o}\in\mathbbm{R}^{p\times p}$ 
        is diagonal.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Control Law}
    The adaptive control law to replace the LTI one is
    \begin{equation*}
        u_{i}(t)=k_{i}(t)y_{i}(t), \forall i\in\mathbbm{I}_{p}\ \iff u(t)=K(t)y(t).
    \end{equation*}
    The adaptation rule is composed of two elements 
    \begin{align*}
         & \dot{k_{i}}(t)=proj_{\mathcal{E}(k_{ic},r_{i})}(k_{i}(t),h_{i}(t)), \\
         & h_{i}(t)=-g_{i}G_{i}y(t)y_{i}(t)-\sigma_{i}(k_{i}(t)-k_{oi}).
    \end{align*}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{equation*}
                k_{i}\in\mathcal{E}(k_{ic},r_{i})  \iff k_{i}\in [k_{ic}-r_{i}\ \ k_{ic}+r_{i}]
            \end{equation*}
        \end{column}
        \begin{column}{0.3\textwidth}
            \includegraphics[width=\columnwidth]{proj.png}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \begin{block}{Assumption}
        Each parameter $\textcolor{black}{\xi_{i}}(t)$ is continuously differentiable and has a bounded derivative, namely there exists
    constants $\overline{\Delta}_{i}\geq0$ and $\underline{\Delta}_{i}\leq0$ such that

    \begin{equation*}
        \label{bound_1}
        \underline{\Delta}_{i} \leq\textcolor{black}{\textcolor{black}{\dot{\textcolor{black}{\xi}}}_{i}}(t)\leq \overline{\Delta}_{i}\, \forall\ i\in\mathbbm{I}_{v}
    \end{equation*}
    \end{block}
    I tackled this problem in two ways.
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Affine Overbounding}
    \begin{lemma}
        %
        Given a function $P:\mathbbm{E}^{v}\to\mathbbm{S}^{n}$ defined as
        \begin{equation*}
            P(\textcolor{black}{\xi})=\sum_{i=1}^{v}\textcolor{black}{\xi_{i}}P^{[i]},
        \end{equation*}
        With $P_{i}\in\mathbbm{S}_{+}^{n}\forall i \in\mathbbm{I}_{v}$, under the just defined Assumption, there exist a scalar $\overline{\Delta}$ such that
        \begin{equation*}
            \dot{P}(\textcolor{black}{\xi})\leq\overline{\Delta}\sum_{i=1}^{v}P^{[i]}
        \end{equation*}
    \end{lemma}
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Multiaffine derivative polytope}
    A multiaffine description of the matrix $P(\xi)$ is the starting point
    \begin{equation*}
        P(\xi)=\sum_{j_{1}=1}^{2}\dots\sum_{j_{n_{i}}=1}^{2}\xi_{1j_{1}}\dots\xi_{n_{i}j_{n_{i}}}P^{[j_{1} , \dots , j_{n_{i}}]}.
    \end{equation*}
    Being each $\xi_{i}\in\mathbbm{E}^{2}$, the following also holds true
    \begin{equation*}
        \dot{\xi}_{i1}=-\dot{\xi}_{i2},\ \forall i\in\mathbbm{I}_{n_{i}}
    \end{equation*}
    Here the bounds of $\dot{\xi}_{ij}$ will not be $[\underline{\Delta}_{i}\ \overline{\Delta}_{i}]$ anymore, but 
    $[\underline{\delta}_{i}\ \overline{\delta}_{i}]$.
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Multiaffine derivative polytope}
    We can use this notation to compute the derivative of $\dot{P}(\xi)$
    \begin{equation*}
        \resizebox*{.9\hsize}{!}{$
        \dot{P}(\xi)= \sum_{k=1}^{n_{i}}(\sum_{j_{1}=1}^{2}\dots\sum_{j_{k-1}=1}^{2}\sum_{j_{k+1}=1}^{2}\dots\sum_{j_{n_{i}}=1}^{2}\xi_{1j_{1}}\dots
    \xi_{n_{i}j_{n_{i}}}\dot{\xi}_{k1}P_{d}^{[j_{1} , \dots , j_{k-1} , j_{k+1} , \dots , j_{n_{i}}]}),
        $}
    \end{equation*}
    where 
    \begin{equation*}
        P_{d}^{[j_{1} , \dots , j_{i-1} , j_{i+1} , \dots , j_{n_{i}}]}=(P^{[j_{1} , \dots , j_{i-1} , 1 , j_{i+1} , \dots , j_{n_{i}}]}-P^{[j_{1} , \dots , j_{i-1} , 2 , j_{i+1} , \dots , j_{n_{i}}]})
    \end{equation*}
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Multiaffine derivative polytope}
    Furthermore being $\dot{\xi}_{k1}=\underline{\delta}_{k}\phi_{1k}+\overline{\delta}_{k}\phi_{2k}=\sum_{l_{k}=1}^{2}\delta_{l_{k}k}\phi_{l_{k}k}$, we can 
    build all the vertexes of $\dot{P}(\xi)$ to show its multiaffine nature in this way
    \begin{equation*}
        \resizebox*{.9\hsize}{!}{$
        \dot{P}^{[l_{1} , \dots , l_{n_{i}}]}=  \sum_{k=1}^{n_{i}}(\sum_{j_{1}=1}^{2}\dots\sum_{j_{k-1}=1}^{2}\sum_{j_{k+1}=1}^{2}\dots\sum_{j_{n_{1}}=1}^{2}
    \xi_{1j_{1}}\dots\xi_{n_{i}j_{n_{i}}}\delta_{l_{k}k}P_{d}^{[j_{1} , \dots , j_{k-1} , j_{k+1} , \dots , j_{n_{i}}]})
        $}
    \end{equation*}
    where $\delta_{l_{1} , \dots , l_{n_{i}}}=[\delta_{l_{1}1} , \dots , \delta_{l_{n_{i}}n_{i}}]$ is the vector of all 
    multiaffine polytopic parameters bounds.
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Multiaffine derivative polytope}
    Finally the derivative of $P(\xi)$ takes the form 
    \begin{equation*}
        \dot{P}(\xi)=\dot{P}(\phi_{1}\dots\phi_{n_{i}})=\sum_{k_{1}=1}^{2}\dots\sum_{k_{n_{i}}=1}^{2}\phi_{1k_{1}}\dots\phi_{n_{i}k_{n_{i}}}\dot{P}^{[k_{1} , \dots , k_{n_{i}}]}
    \end{equation*}
\end{frame}
\begin{frame}
    \frametitle{Modeling Variable Uncertainties}
    \framesubtitle{Multiaffine derivative polytope: Example}
    Vertexes of a polytope defined by 2 uncertanties:
    \begin{align*}
        & \dot{P}^{[1,1]}=\sum_{j_{1}=1}^{2}\xi_{j_{1}}\underline{\delta}_{2}(P^{[j_{1} , 1]}-P^{[j_{1} , 2]})
       +\sum_{j_{2}=1}^{2}\xi_{j_{2}}\underline{\delta}_{1}(P^{[1 , j_{2}]}-P^{[2 , j_{1}]})                   \\
        & \dot{P}^{[2,1]}=\sum_{j_{1}=1}^{2}\xi_{j_{1}}\underline{\delta}_{2}(P^{[j_{1} , 1]}-P^{[j_{1} , 2]})
       +\sum_{j_{2}=1}^{2}\xi_{j_{2}}\overline{\delta}_{1}(P^{[1 , j_{2}]}-P^{[2 , j_{1}]})                    \\
        & \dot{P}^{[1,2]}=\sum_{j_{1}=1}^{2}\xi_{j_{1}}\overline{\delta}_{2}(P^{[j_{1} , 1]}-P^{[j_{1} , 2]})
       +\sum_{j_{2}=1}^{2}\xi_{j_{2}}\underline{\delta}_{1}(P^{[1 , j_{2}]}-P^{[2 , j_{1}]})                   \\
        & \dot{P}^{[2,2]}=\sum_{j_{1}=1}^{2}\xi_{j_{1}}\overline{\delta}_{2}(P^{[j_{1} , 1]}-P^{[j_{1} , 2]})
       +\sum_{j_{2}=1}^{2}\xi_{j_{2}}\overline{\delta}_{1}(P^{[1 , j_{2}]}-P^{[2 , j_{1}]}).
   \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Common Matrices}
    Before stating the new results we show a few matrices used in the theorems to come 
    \begin{align}
        \label{datamatrices}
        N_{1x}                  & =[I_{q}\ \ 0_{q,n_{x}}\ \ 0_{q,m_{w}}\ \ 0_{q,p}], & N_{2x}                        & =[0_{n_{x},q}\ \ I_{n_{x}}\ \ 0_{n_{x},m_{w}}\ \ 0_{n_{x},p}],               \nonumber \\
        N_{z}                   & =[0_{p_{z},q}\ \ C_{z}\ \ D_{zw}\ \ 0_{p_{z},p}],  & N_{w}                         & =[{0_{m_{w},q}\ \ 0_{m_{w},n_{x}}\ \ I_{m_{w}}\ \ 0_{m_{w},p}}], \nonumber             \\
        \textcolor{black}{N_{K}} & =[\textcolor{black}{K_{c}}-K_{o}\ \ I_{p}],         & \textcolor{black}{N_{F}^{[i]}} & =[\textcolor{black}{F^{[i]}}-K_{o}\ \ I_{p}],     \nonumber                             \\
        N_{y}                   & =[0_{p,q}\ \ C_{y}\ \ 0_{p,m_{w}}\ \ 0_{p,p}],     & N_{yu}                        & =\begin{bmatrix} 0_{p,q} & C_{y} & 0_{p,m_{w}} & 0_{p,p}\\ 0_{p,q} & 0_{p,p} & 0_{p,m_{w}} & -I_{p} \end{bmatrix}.
    \end{align}
    \begin{align}
        \label{modification}
        M_{c}^{[i]}                   & =[E_{1}^{[i]}\ \ -(A^{[i]}+B_{u}^{[i]}K_{o}C_{y})\ \ -B_{w}^{[i]}\ \ -B_{u}^{[i]}],\nonumber  \\
        \textcolor{black}{P_{e}^{[i]}} & =(E_{2}^{T}\textcolor{black}{P^{[i]}}+\textcolor{black}{Y^{[i]}}E_{2}^{\perp})E_{2x\pi}^{\perp}
    \end{align}
\end{frame}
\begin{frame}
    \frametitle{Previous results}
    \framesubtitle{\tiny Dimitri Peaucelle, Harmonie Leduc. Adaptive control design with S-variable LMI approach 
    for robustness and L2 performance. International Journal of Control, Taylor $\&$ Francis, 2020, 93 (2), pp.194-203.}
    For all $2^{n_{i}}$ vertexes these LMIs are defined
    \begin{align}
        %
        \label{raggio1}
        \begin{bmatrix}
            \textcolor{black}{R^{2}}         & \textcolor{black}{K_{c}-F^{[i]}} \\
            \textcolor{black}{K_{c}-F^{[i]}} & I
        \end{bmatrix}\geq0 \\
        \label{raggio2}
        \begin{bmatrix}
            \textcolor{black}{R^{2}}       & \textcolor{black}{K_{c}}-K_{o} \\
            \textcolor{black}{K_{c}}-K_{o} & I
        \end{bmatrix}\geq0
    \end{align}
    %
    \begin{equation}
        \label{pdefinite}
        (E_{2}E_{2}^{o})^{T}\textcolor{black}{P^{[i]}}(E_{2}E_{2}^{o})>0
    \end{equation}
\end{frame}
\begin{frame}
    \frametitle{Previous results}
    \framesubtitle{\tiny Dimitri Peaucelle, Harmonie Leduc. Adaptive control design with S-variable LMI approach 
    for robustness and L2 performance. International Journal of Control, Taylor $\&$ Francis, 2020, 93 (2), pp.194-203.}
    \begin{block}{Remark}
        Here the uncertain parameters $\xi$ are considered constant.
    \end{block}
    For all $2^{n_{i}}$ vertexes these LMIs are defined
    \begin{multline}
        \label{t4}
        \{N_{2x}^{T}P_{e}^{[i]}N_{1x}+S M_{c}^{[i]}\}^{S} +\epsilon N_{2x}^{T}E_{2}^{T}E_{2}N_{2x}+\alpha (N_{z}^{T}N_{z}-\hat{\gamma}^{2} N_{w}^{T}N_{w})\\
        +2N_{y}^{T}R^{2}N_{y}-2N_{yu}^{T}N_{K}^{T}N_{K}N_{yu}+\{N_{y}^{T}G^{T}N_{F}^{[i]}N_{yu}\}^{S}<0
    \end{multline}
\end{frame}
\begin{frame}
    \frametitle{Previous results}
    \framesubtitle{\tiny Dimitri Peaucelle, Harmonie Leduc. Adaptive control design with S-variable LMI approach 
    for robustness and L2 performance. International Journal of Control, Taylor $\&$ Francis, 2020, 93 (2), pp.194-203.}
    \begin{theorem}
        {\tiny Considering the system \eqref{sistema} in closed loop with the adaptive control law surveyed before,
        if the matrix inequalities \eqref{raggio1}\eqref{raggio2}\eqref{pdefinite}\eqref{t4} are feasible for all vertices $i\in\mathbbm{I}_{v}$
              where $S$, $R^{2}=diag(r^{2})$, $G$, $K_{c}=diag(k_{c})$, $\hat{\gamma}$, $\epsilon$ and $\alpha$ are decision variables
              common to all inequalities, while $P^{[i]}=P^{[i]^{T}}$, $Y^{[i]}$, $F^{[i]}=diag(f^{[i]})$ are vertex-dependent
              decision variables, then whatever choice og $\Gamma=diag(g)>0$, $\Sigma=diag(\sigma)\geq0$, the system
               is such that}
              \begin{equation*}
                \resizebox*{0.7\hsize}{!}{$
                \dot{V}(x,K,\xi)+2\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\hat{\gamma}^{2}w^{T}w)<8\sum_{i=1}^{p}\sigma_{i}\frac{r_{i}^{2}}{g_{i}}
                $}
              \end{equation*}
              {\tiny where} \resizebox*{0.1\hsize}{!}{$V(x,K,\xi)$} {\tiny is a parameter-dependent Lyapunov function defined by}
              \begin{equation*}
                \resizebox{0.7\hsize}{!}{$
                V(x,K,\xi)=x^{T}E_{2}^{T}P(\xi)E_{2}x+Tr[(K-F(\xi))^{T}\Gamma^{-1}(K-F(\xi))]
                $}
              \end{equation*}
              {\tiny with $P(\xi)=\sum_{i=1}^{2^{n_{i}}}\xi_{i}P^{[i]}$ and $F(\xi)=\sum_{i=1}^{2^{n_{i}}}\xi_{i}F^{[i]}$.}
    \end{theorem}
\end{frame}
\begin{frame}
    \frametitle{New Set of Conditions}
    \framesubtitle{First set: affine bound}
    %
    \begin{block}{Assumption}
        In the new sets of conditions we will not consider $\dot{\xi}$ to be equal to 0 anymore.
    \end{block}
    While keeping the inequalities concerning the adaptive control from the previous result, a new LMI is defined for all 
    $2^{n_{i}}$ vertexes
    \begin{multline}
        \label{LMI_1}
        \resizebox*{\hsize}{!}{$\{N_{2x}^{T}\textcolor{black}{P_{e}^{[i]}}N_{1x}+\textcolor{black}{S}M_{c}^{[i]}\}^{S} +N_{2x}^{T}E_{2}^{T}(\textcolor{black}{\epsilon} I+\textcolor{red}{\overline{\Delta}\sum_{i=1}^{v}P^{[i]}})E_{2}N_{2x}+\textcolor{black}{\alpha} (N_{z}^{T}N_{z}-\textcolor{black}{\gamma^{2}}N_{w}^{T}N_{w})$}\\
        +2N_{y}^{T}\textcolor{black}{R^{2}}N_{y}-2N_{yu}^{T}\textcolor{black}{N_{K}^{T}N_{K}}N_{yu}+\{N_{y}^{T}\textcolor{black}{G^{T}N_{F}^{[i]}}N_{yu}\}^{S}<0
    \end{multline}
\end{frame}
\begin{frame}
    \frametitle{New Set of Conditions}
    \framesubtitle{First set: affine bound}
    \begin{theorem}
        %
        \label{theorem1}
        {\small Given the matrices in \eqref{datamatrices}\eqref{modification}, assume that there exists decision variables $S$, $R^{2}=diag(r^{2})$, $G$, $K_{c}=diag(k_{c})$,
        $\gamma$, $\epsilon$, $\alpha$ common to all vertices and and $P^{[i]}={P^{[i]}}^{T}$, $Y^{[i]}$, $F^{[i]}=diag(f^{[i]})$ vertex-dependent
        decision variables, then if for all vertices $\in i=\mathbbm{I}_{v}$ satify the LMIs \eqref{raggio1}\eqref{raggio2}\eqref{pdefinite}\eqref{LMI_1}
        then for any choice of $\Gamma=diag(g)>0$, $\Sigma=diag(\sigma)\geq0$, system \eqref{sistema} in closed loop with
        the adaptive control previously defined is such that}
        \begin{equation}
            \label{result1}
            \resizebox*{0.7\hsize}{!}{$
            \dot{V}(x,K,\textcolor{black}{\xi})+\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}w^{T}w)
            <8p\frac{\bar{\sigma}\bar{r}^{2}}{\underline{g}}+4pv\frac{\overline{\overline{\Delta}}}{\underline{g}}(\bar{r}^{2}+|\bar{k}_{c}\bar{r}|)
            $}
        \end{equation}
        {\small Where $V(x,K,\textcolor{black}{\xi})$ is a parameter-dependent Lyapunov function defined as}
        \begin{equation}
            \label{lyap}
            \resizebox*{.7\hsize}{!}{$
            V(x,K,\textcolor{black}{\xi})=x^{T}E_{2}^{T}P(\textcolor{black}{\xi})E_{2}x+Tr[(K-F(\textcolor{black}{\xi}))^{T}\Gamma^{-1}(K-F(\textcolor{black}{\xi}))]
            $}
        \end{equation}
        and $\overline{\Delta}$, $\overline{\overline{\Delta}}$ are quantities defined in Lemma \ref{lemma1}.
    \end{theorem}
\end{frame}
\begin{frame}
    \frametitle{New Set of Conditions}
    \framesubtitle{Second set: multiaffine bound}
    While for all $2^{n_{i}}$ vertexes we still keep the inequalities concerning the adaptive law and the PD discussed before (in multiaffine notation),
    for each vertex we define $2^{2n_{i}}$ new LMIs in this way
    \begin{align}
        \label{LMI_1bis}
         & \{N_{2x}^{T}\textcolor{black}{P_{e}^{[j_{1} , \dots , j_{n_{i}}]}}N_{1x}+\textcolor{black}{S}M_{c}^{[j_{1} , \dots , j_{n_{i}}]}\}^{S}+\textcolor{black}{\alpha} (N_{z}^{T}N_{z}-\textcolor{black}{\gamma^{2}}N_{w}^{T}N_{w})+\nonumber \\
         & N_{2x}^{T}E_{2}^{T}(\textcolor{black}{\epsilon} I+
        \textcolor{red}{\sum_{k=1}^{n_{i}}\delta_{l_{k}1}P_{d}^{[j_{1} , \dots , j_{k-1} , j_{k+1} , \dots , j_{n_{i}}]}})E_{2}N_{2x}+\nonumber                                                                                                                                                    \\
         & 2N_{y}^{T}\textcolor{black}{R^{2}}N_{y}-2N_{yu}^{T}\textcolor{black}{N_{K}^{T}N_{K}}N_{yu}+\{N_{y}^{T}\textcolor{black}{G^{T}N_{F}^{[j_{1} , \dots , j_{n_{i}}]}}N_{yu}\}^{S}<0.
    \end{align}
\end{frame}
\begin{frame}
    \frametitle{New Set of Conditions}
    \framesubtitle{Second set: multiaffine bound}
    \begin{theorem}
        {\scriptsize Given the matrices in \eqref{datamatrices}\eqref{modification}, assume that there exists decision variables $S$, $R^{2}=diag(r^{2})$, $G$, $K_{c}=diag(k_{c})$,
        $\gamma$, $\epsilon$, $\alpha$ common to all vertices and $P_{e}^{[j_{1} , \dots , j_{n_{i}}]}={P_{e}^{[j_{1} , \dots , j_{n_{i}}]}}^{T}$, 
        $Y^{[j_{1} , \dots , j_{n_{i}}]}$, $F^{[j_{1} , \dots , j_{n_{i}}]}=diag(f^{[j_{1} , \dots , j_{n_{i}}]})$ vertex-dependent
        decision variables, then if for all vertices $[j_{1}, \dots j_{i}, \dots j_{n_{i}}]$ with $i\in\mathbbm{I}_{v}$ satify the LMIs \eqref{raggio1}\eqref{raggio2}\eqref{pdefinite}\eqref{LMI_1}
        then for any choice of $\Gamma=diag(g)>0$, $\Sigma=diag(\sigma)\geq0$, system \eqref{sistema} in closed loop with
        the adaptive control previously defined is such that}
        \begin{equation}
            \label{result1}
            \resizebox*{.7\hsize}{!}{$
            \dot{V}(x,K,\textcolor{black}{\xi})+\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}w^{T}w)
            <8\frac{\overline{r}^{2}p}{\underline{g}}(\overline{\sigma}+\overline{\overline{\delta}})
            $}
        \end{equation}
        {\small Where $V(x,K,\textcolor{black}{\xi})$ is a parameter-dependent Lyapunov function defined as}
        \begin{equation}
            \label{lyap}
            \resizebox*{.7\hsize}{!}{$
            V(x,K,\textcolor{black}{\xi})=x^{T}E_{2}^{T}P(\textcolor{black}{\xi})E_{2}x+Tr[(K-F(\textcolor{black}{\xi}))^{T}\Gamma^{-1}(K-F(\textcolor{black}{\xi}))]
            $}.
        \end{equation}
    \end{theorem}
\end{frame}
\begin{frame}
    \frametitle{New Set of Conditions}
    \framesubtitle{Sketch of Proof}
    \begin{itemize}
        \item Take the convex sum over both the parameters and derivative polytopes, this will sum the red part exactly to 
                $\dot{P}(\xi)$ 
        \item Apply a congruence transformation using the vector $\mu=\begin{pmatrix}E_{2xx}\dot{x}+E_{2x\pi}\pi\\ x \\ w \\ (K-K_{o})y \end{pmatrix}$
        \item Compute $\dot{V}(x,K,\xi)$ and manipulate algebraically
        \item Red part will cancel out with $x^{T}E_{2}^{T}\dot{P}(\xi)E_{2}x$
        \item Bound using the properties of the adaptive control
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Performance and Stability}
    \framesubtitle{Practical Stability}
    Calling the bound to $\dot{V}(x,K,\textcolor{black}{\xi})+\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}w^{T}w)$  $\mathbbm{a}$, we define 2 scalars
    \begin{equation*}
        l_{1}=\frac{\lambda_{max}(P(\xi))}{\epsilon}(\mathbbm{a})+4p\frac{\overline{\sigma}}{\underline{g}}
    \end{equation*}
    \begin{equation*}
        l_{2}^{2}=\max_{\xi\in\mathbbm{E}^{v}}\frac{l_{1}(\xi)}{\lambda_{min}(P(\xi))}
    \end{equation*}
\end{frame}
\begin{frame}
    \frametitle{Performance and Stability}
    \framesubtitle{Practical Stability}
    \begin{itemize}
        \item $l_{1}$ is such that $V(x,k,\xi)\geq l_{1}$ implies $\dot{V}(x,k,\xi)<0$. This means that every trajectory starting out of $V\leq l_{1}$ 
        by LaSalle Invariance Principle will converge to this set.
        \item $l_{2}$ is such that if $V\leq l_{1}$ then $x^{T}E_{2}^{T}E_{2}x\leq l_{2}^{2}$. This means that $||E_{2}x||$ is asymptotically less than $l_{2}$ 
        robustly, in other words the state converges to this neighborhood of the nullspace $E_{2}x=0$.
    \end{itemize}
    We call this last propriety practical stability.
\end{frame}
\begin{frame}
    \frametitle{Performance and Stability}
    \framesubtitle{Practical $L_{2}$ Performance}
    Studying the input-output performance of the system, we define 
    \begin{equation*}
        ||z||_{T}^{2}=\int_{0}^{T}z^{T}(t)z(t)dt.
    \end{equation*}
    Assuming that $x(0)=0$, $k(0)=k_{c}$, we get
    \begin{equation*}
        ||z||_{T}^{2}<\gamma||w||_{T}^{2}+\frac{\overline{r}^{2}}{\underline{g}\alpha}+\frac{T\mathbbm{a}}{\alpha}.
    \end{equation*}
    This bound is called practical $L_{2}$ performance.
\end{frame}


\begin{frame}
    \frametitle{Conclusions and Further Works}
    \begin{itemize}
        \item New result on the performance of variable uncertain systems
        \item Two different sets of conditions each with defined advantages: multiaffine is 
            less conservative while affine is less computationally intensive
        \item Possible enhancements by changing Lyapunov's function and/or control law
    \end{itemize}
\end{frame}

\end{document}