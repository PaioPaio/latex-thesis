\documentclass{article}
\usepackage{amssymb}
\usepackage{bbm}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{microtype}
\usepackage{xcolor}
%======================================================================================

\newtheorem{assumption}{Assumption}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{theorem}{Theorem}

\newenvironment{proof}{\noindent {\em Proof.}}{\hfill \hspace*{1pt}\hfill $\square$\\}

%======================================================================================
\title{Bound on SV-LMI condition with slowly time-varying $\xi(t)$ }
\date{\today}
\author{Lorenzo Paiola}

%======================================================================================
\begin{document}
\maketitle
\pagenumbering{arabic}
\begin{abstract}
    Following the same framework of the paper \cite{peaucelle_adaptive_2018} a generalization of the result is proposed 
    through the introduction of a bound on the derivative of $\xi_{i}(t)$ which is a vector of time-dependent parameters 
    representing the uncertainty on the actual parameters of the polytopic system taken in consideration. This will also 
    introduce a new bound on the performances and a new slightly modified LMI system.
\end{abstract}

%======================================================================================

\section{Problem Statement}
\label{sec:PS}

The generalization we want to add to the paper \cite{peaucelle_adaptive_2018} is about the parameters $\xi$ of the polytopic 
system, those are assumed to be constant in the aforementioned paper, but we relax that assumption to considering slow-varying 
functions $\xi(t) \in E^p$ with
%
\begin{equation*}
    \ E^p : = \{\xi(t) \in \mathbb{R}^{p}|\xi(t)\geq0,\mathbbm{1}^T\xi(t)=1\}
\end{equation*}
Which clearly implies
\begin{equation*}
    \xi_{i}(t)\in[0,1]\, \forall\, i = 1, \dots ,p.
\end{equation*}
The system under consideration is in descriptor form and corresponds to
\begin{align}
    \label{system}
    E_{x}(\xi)\dot{x}(t)+E_{\pi}(\xi)\pi(t) & =A(\xi)x(t)+B_{\omega}\omega(t)+B_{u}(\xi)u(t) \\
    \label{out}
    y(t)=C_{y}x(t)\, , z(t)\,                & =C_{z}x(t)+D_{z\omega}\omega(t)
\end{align}
where $x(t)\in\mathbb{R}^{n_{x}}$ is the state, $\omega(t)\in\mathbb{R}^{m_{\omega}}$ is a perturbation, $u(t)\in\mathbb{R}^{m_{u}}$
is the control input and $\pi(t)\in\mathbb{R}^{n_{\pi}}$ is the auxiliary signal, $y(t)\in\mathbb{R}^{p}$ and $z(t)\in\mathbb{R}^{p_{z}}$
are the measurements and performance outputs. We also have an output-feedback control $u=K_{o}y$ that was already designed to be 
stabilizing and to guarantee a certain performance.
By adding extra redundant inputs and outputs, the system can be rewritten in a square form with $p=m_{u}$ and $K_{o}=diag(k_{o})$. 
In \cite{peaucelle_adaptive_2018} it is also proved that we can do this without any loss of generality.
Let us assume the following sector bound for the variation of $\xi(t)$.


\begin{assumption}
    \label{Assumption1}

    Each parameter $\xi_{i}(t)$ is continuously differentiable and has a "sector-bounded" derivative, namely there exists 
    a constant $n_{i}\geq0$ such that

    \begin{equation*}
        |\dot\xi_{i}(t)|\leq n_{i}|\xi_{i}(t)|
    \end{equation*}
    Since the $\xi_{i}(t)\geq0$ by assumption, the last inequality can be rewritten as
    \begin{equation}
        \label{bound1}
        -n_{i}\xi_{i}(t)\leq \dot\xi_{i}(t)\leq n_{i}\xi_{i}(t)
    \end{equation}
\end{assumption}
%
From the Assumption \ref{Assumption1} just stated we can prove a lemma.
%
%
\begin{lemma}
    %
    For a function $g(\xi(t))$ defined as a convex combination in such a way
    \begin{equation*}
        \forall\ g(\xi(t))=\sum_{i=1}^{p}\xi_{i}(t)g_{i}\ :\mathbb{R}^p \mapsto \mathbb{R}^{n\times m}
    \end{equation*}
    If $g_{i}\geq0\ \forall i\in\{1,2...p\}$, there exist a scalar $\bar{n}$ such that
    \begin{equation}
        \label{lemma1}
        \dot{g}(\xi(t))\leq\bar{n}g(\xi(t))
    \end{equation}
\end{lemma}
%
%
\begin{proof}
    Taking the time derivative of $g(\xi(t))$ and using \eqref{bound1} we can write
    \begin{equation*}
        \dot{g}(\xi(t))=\sum_{i=1}^{p}\dot{\xi}_{i}(t)g_{i}\leq\sum_{i=1}^{p}n_{i}\xi_{i}(t)g_{i}
    \end{equation*}
    Taking $\bar{n}=\max\{n_{1},n_{2},\ldots,n_{p}\}$, it follows that
    \begin{equation*}
        \dot{g}(\xi(t))\leq\sum_{i=1}^{p}n_{i}\xi_{i}(t)g_{i}\leq\sum_{i=1}^{p}\bar{n}\xi_{i}(t)g_{i}=\bar{n}g(\xi(t))
    \end{equation*}
    Which is the relation we were looking for.
\end{proof}
%
%
\section{Main Result}
%
Taking in consideration the LMI and control architecture in the main result \cite{peaucelle_adaptive_2018}, a slight modification is applied
to accommodate for the variation of the $\xi_{i}(t)$ parameters. The inequalities are now reported for convenience of the reader.
\begin{align}
    %
    \label{raggio1}
    \begin{bmatrix}
        R^{2}         & K_{c}-F^{[v]} \\
        K_{c}-F^{[v]} & I
    \end{bmatrix}\geq0 \\
    \label{raggio2}
    \begin{bmatrix}
        R^{2}       & K_{c}-K_{o} \\
        K_{c}-K_{o} & I
    \end{bmatrix}\geq0
\end{align}
%
\begin{equation}
    \label{pdefinite}
    (E_{2}E_{2}^{o})^{T}P^{[v]}(E_{2}E_{2}^{o})>0
\end{equation}
%
\begin{align}
    \label{LMI}
    \{N_{2x}^{T}P_{e}^{[v]}N_{1x}+SM_{c}^{[v]}\}^{S}+\epsilon N_{2x}^{T}E_{2}^{T}E_{2}N_{2x}+\alpha (N_{z}^{T}N_{z}-\gamma^{2}N_{\omega}^{T}N_{\omega})\nonumber \\
    +2N_{y}^{T}R^{2}N_{y}-2N_{yu}^{T}N_{K}^{T}N_{K}N_{yu}+\{N_{y}^{T}G^{T}N_{F}^{[v]}N_{yu}\}^{S}<0
\end{align}
%
\begin{align*}
    N_{1x} & =[I_{q}\ \ \textcolor{red}{\frac{\bar{n}}{2}E_{2xx}}\ \ 0_{q,m_{\omega}}\ \ 0_{q,p}], & N_{2x}      & =[0_{n_{x},q}\ \ I_{n_{x}}\ \ 0_{n_{x},m_{\omega}}\ \ 0_{n_{x},p}],                  \\
    N_{z}  & =[0_{p_{z},q}\ \ C_{z}\ \ D_{z\omega}\ \ 0_{p_{z},p}],                                & N_{\omega}  & =[{0_{m_{\omega},q}\ \ 0_{m_{\omega},n_{x}}\ \ I_{m_{\omega}}\ \ 0_{m_{\omega},p}}], \\
    N_{K}  & =[K_{c}-K_{o}\ \ I_{p}],                                                              & N_{F}^{[v]} & =[F^{[v]}-K_{o}\ \ I_{p}],                                                           \\
    N_{y}  & =[0_{p,q}\ \ C_{y}\ \ 0_{p,m_{\omega}}\ \ 0_{p,p}],                                   & N_{yu}      & =\begin{bmatrix} 0_{p,q} & C_{y} & 0_{p,m_{\omega}} & 0_{p,p}\\ 0_{p,q} & 0_{p,p} & 0_{p,m_{\omega}} & -I_{p} \end{bmatrix},                                                         \\
\end{align*}
%
\begin{align}
    \label{modification}
    M_{c}^{[v]} & =[E_{1}^{[v]}\ \ -(A^{[v]}+B_{u}^{[v]}K_{o}C_{y})\ \ -B_{\omega}^{[v]}\ \ -B_{u}^{v}],\nonumber \\
    P_{e}^{[v]} & =(E_{2}^{T}P^{[v]}+Y^{[v]}E_{2}^{\perp})E_{2x\pi}^{\perp}
\end{align}
%
\begin{theorem}
    %
    If with this new choice of matrices the LMI \eqref{raggio1}\eqref{raggio2}\eqref{pdefinite}\eqref{LMI} is feasible for all vertices
    v=1...$\bar{v}$ where S, $R^{2}=diag(r^{2})$, G, $K_{c}=diag(k_{c})$, $\gamma$, $\epsilon$, $\alpha$ are decision variables common to all
    inequalities, and $P^{[v]}={P^{[v]}}^{T}$, $Y^{[v]}$, $F^{[v]}=diag(f^{[v]})$ are vertex-dependent decision variables, then whatever choice
    of $\Gamma=diag(g)>0$, $\Sigma=diag(\sigma)\geq0$, the system \eqref{system}\eqref{out} in closed loop with the adaptive control defined
    in \cite{peaucelle_adaptive_2018} has the following property
    \begin{equation}
        \label{result1}
        \dot{V}(x,K,\xi)+2\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}\omega^{T}\omega)<8p\frac{\bar{\sigma}\bar{r}^{2}}{\underline{g}}+4\frac{\bar{n}p}{\underline{g}}(\bar{r}^{2}+|\bar{k_{c}}\bar{r}|)
    \end{equation}
    Where V(x,K,$\xi$) is a parameter-dependent Lyapunov function defined as
    \begin{equation}
        \label{lyap}
        V(x,K,\xi)=x^{T}E_{2}^{T}P(\xi)E_{2}x+Tr[(K-F(\xi))^{T}\Gamma^{-1}(K-F(\xi))]
    \end{equation}
\end{theorem}
%
%
\begin{proof}
    Consider the following vector
    \begin{equation*}
        \mu=\begin{pmatrix}E_{2xx}\dot{x}+E_{2x\pi}\pi\\ x \\ \omega \\ (K-K_{o})y \end{pmatrix}
    \end{equation*}
    a congruence transformation on the LMI rewrites it to
    \begin{align}
        \label{eq1}
         & 2\dot{x}^{T}E_{2}^{T}P(\xi)E_{2}x+2\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}\omega^{T}\omega)\nonumber \\
         & +2y^{T}(R-(K_{c}-K)^{T}(K_{c}-K)+G^{T}(F(\xi)-K)y)+\bar{n}x^{T}E_{2}^{T}E_{2}x<0
    \end{align}
    While taking the derivative of the Lyapunov function gives
    \begin{align}
        \label{eq2}
        \dot{V} & =2\dot{x}^{T}E_{2}^{T}P(\xi)E_{2}x+2\sum_{i=1}^{p}(-G_{i}yy_{i}-\frac{\sigma_{i}}{g_{i}}(k_{i}-k_{oi})+g_{i}^{-1}q_{i})(k_{i}-f_{i}(\xi))\nonumber \\
                & +2\sum_{i=1}^{p}-\dot{f}_{i}(\xi)(k_{i}-f_{i}(\xi))g_{i}^{-1}+x^{T}E_{2}^{T}\dot{P}(\xi)E_{2}x
    \end{align}
    Now substituting \eqref{eq2} in \eqref{eq1} and reusing the results in \cite{peaucelle_adaptive_2018}
    \begin{align}
        \label{eq3}
        \dot{V} & +2\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}\omega^{T}\omega)+\bar{n}x^{T}E_{2}^{T}P(\xi)E_{2}x\nonumber \\&<8\frac{\bar{\sigma}}{\underline{g}}p\bar{r}^{2}+x^{T}E_{2}^{T}\dot{P}(\xi)E_{2}x+2\sum_{i=1}^{p}-\dot{f}_{i}(\xi)(k_{i}-f_{i}(\xi))g_{i}^{-1}
    \end{align}
    Unfortunately the lemma \eqref{lemma1} can't be used because $f_{ij}\geq0$ for every $f_{i}$ isn't always true, so absolute values have to be used for the bond in such a way
    \begin{align*}
        |\dot{f}_{i}(\xi)| & =|\sum_{j=1}^{p}\dot{\xi}_{j}(t)f_{ij}|\leq\sum_{j=1}^{p}|\dot{\xi}_{j}(t)||f_{ij}|\leq
        \sum_{j=1}^{p}|n_{j}\xi_{j}(t)||f_{ij}|                                                                      \\&\leq\sum_{j=1}^{p}|\bar{n}\xi_{j}(t)||f_{ij}|=\bar{n}\sum_{j=1}^{p}\xi_{j}(t)|f_{ij}|\leq
        \bar{n}\sum_{j=1}^{p}\xi_{j}(t)|\bar{f_{i}}|                                                                 \\&=\bar{n}|\bar{f_{i}}|\leq\bar{n}(|\bar{r}_{i}|+|\bar{k}_{ic}|)
    \end{align*}
    Using this result and applying \eqref{lemma1} in \eqref{eq3} yields
    \begin{align*}
        \eqref{eq3} & <8\frac{\bar{\sigma}}{\underline{g}}p\bar{r}^{2}+x^{T}E_{2}^{T}\dot{P}(\xi)E_{2}x+2\sum_{i=1}^{p}|\dot{f}_{i}(\xi)||k_{i}-f_{i}(\xi)|\underline{g}^{-1} \\&\leq
        \ldots+2\bar{n}\underline{g}^{-1}\sum_{i=1}^{p}(|\bar{k}_{ic}|+|\bar{r}_{i}|)|2r_{i}|\leq
        \ldots+4\frac{\bar{n}p}{\underline{g}}(|\bar{k}_{c}\bar{r}|+\bar{r}^{2})                                                                                              \\&\leq8\frac{\bar{\sigma}}{\underline{g}}p\bar{r}^{2}+\bar{n}x^{T}E_{2}^{T}P(\xi)E_{2}x+4\frac{\bar{n}p}{\underline{g}}(|\bar{k}_{c}\bar{r}|+\bar{r}^{2})
    \end{align*}
    Which implies
    \begin{equation*}
        \dot{V}+2\epsilon x^{T}E_{2}^{T}E_{2}x+\alpha(z^{T}z-\gamma^{2}\omega^{T}\omega)<8\frac{\bar{\sigma}}{\underline{g}}p\bar{r}^{2}+4\frac{\bar{n}p}{\underline{g}}(|\bar{k}_{c}\bar{r}|+\bar{r}^{2})
    \end{equation*}
    That is \eqref{result1}, the bound that was looked for.
\end{proof}
%
%
\begin{remark}
    The result here showed diverges from the paper \cite{peaucelle_adaptive_2018} only by the highlighted
    element in \eqref{modification} that is $\frac{\bar{n}}{2}E_{2xx}$ instead of a $\emptyset$ matrix,
    so taking $\bar{n}=0$ we get the LMI we started from and that means that the new one should be feasible (?is
    it written clearly enough ?) for we can take $\bar{n}$ as small as desired (this will only mean that the
    parameters will change very slowly).
\end{remark}

\section{Problem Statement v2}
Same thing but with Lipshitz function aka bounded derivative.

\section{Main result v2}
\begin{align}
    %
    \label{raggio1}
    \begin{bmatrix}
        R^{2}         & K_{c}-F^{[v]} \\
        K_{c}-F^{[v]} & I
    \end{bmatrix}\geq0 \\
    \label{raggio2}
    \begin{bmatrix}
        R^{2}       & K_{c}-K_{o} \\
        K_{c}-K_{o} & I
    \end{bmatrix}\geq0
\end{align}

\begin{align}
    \label{LMI}
    \{N_{2x}^{T}P_{e}^{[v]}N_{1x}+SM_{c}^{[v]}\}^{S}+\textcolor{red}{N_{2x}^{T}E_{2}^{T}(\epsilon I+\bar{\Delta}\sum_{v=1}^{p}P^{[v]})E_{2}N_{2x}}+\alpha (N_{z}^{T}N_{z}-\gamma^{2}N_{\omega}^{T}N_{\omega})\nonumber \\
    +2N_{y}^{T}R^{2}N_{y}-2N_{yu}^{T}N_{K}^{T}N_{K}N_{yu}+\{N_{y}^{T}G^{T}N_{F}^{[v]}N_{yu}\}^{S}<0
\end{align}
%
\begin{align*}
    N_{1x} & =[I_{q}\ \ 0_{q,n_{x}}\ \ 0_{q,m_{\omega}}\ \ 0_{q,p}], & N_{2x}      & =[0_{n_{x},q}\ \ I_{n_{x}}\ \ 0_{n_{x},m_{\omega}}\ \ 0_{n_{x},p}],                  \\
    N_{z}  & =[0_{p_{z},q}\ \ C_{z}\ \ D_{z\omega}\ \ 0_{p_{z},p}],  & N_{\omega}  & =[{0_{m_{\omega},q}\ \ 0_{m_{\omega},n_{x}}\ \ I_{m_{\omega}}\ \ 0_{m_{\omega},p}}], \\
    N_{K}  & =[K_{c}-K_{o}\ \ I_{p}],                                & N_{F}^{[v]} & =[F^{[v]}-K_{o}\ \ I_{p}],                                                           \\
    N_{y}  & =[0_{p,q}\ \ C_{y}\ \ 0_{p,m_{\omega}}\ \ 0_{p,p}],     & N_{yu}      & =\begin{bmatrix} 0_{p,q} & C_{y} & 0_{p,m_{\omega}} & 0_{p,p}\\ 0_{p,q} & 0_{p,p} & 0_{p,m_{\omega}} & -I_{p} \end{bmatrix},                                                         \\
\end{align*}
%
\begin{align}
    \label{modification}
    M_{c}^{[v]} & =[E_{1}^{[v]}\ \ -(A^{[v]}+B_{u}^{[v]}K_{o}C_{y})\ \ -B_{\omega}^{[v]}\ \ -B_{u}^{v}],\nonumber \\
    P_{e}^{[v]} & =(E_{2}^{T}P^{[v]}+Y^{[v]}E_{2}^{\perp})E_{2x\pi}^{\perp}
\end{align}

\bibliographystyle{plain}
\bibliography{references}
\end{document}

